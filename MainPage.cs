﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp

    /// <summary>
    /// Class for the Main BackOffice PAge.  This class will throw an exception of the title does not match "Apriva Back Office"
    /// </summary>
    /// <by>Deborah DeVine</by>
    /// <created>06/27/2014</created>
{
    public class MainPage : fitlibrary.DoFixture
    {
        protected IWebDriver driver;


        [FindsBy(How = How.CssSelector, Using = "a[id*='_lbLogout']")]
        protected IWebElement lnkLogout;
       
        [FindsBy(How = How.CssSelector, Using = "a[id*='CurrentLanguage']")]
        protected IWebElement lnkLanguage;

      
        [FindsBy(How = How.CssSelector, Using = "img[id*='ctl00_img1']")]
        protected IWebElement pageLogo;

        public MainPage()
        {
            driver = Test.WebDriver;
            PageFactory.InitElements(driver, this);

            if (driver.Title != "Apriva Back Office")
                throw new NoSuchWindowException("Back Office Page Not Found");
            else
                Console.WriteLine("Apriva Main Page");
       
        }


        /// <summary>
        /// Logs out of the BackOffice Website
        /// </summary>
        /// <returns><c>true</c> if the Login Page is Displayed ; <c>false</c>otherwise.</returns>
        /// <by>Deborah DeVine</by>
        /// <created>06/27/2014</created>
        public Boolean Logout()
        {
            //Logout of Application
            try
            {
                if (lnkLogout.ElementIsClickable())
                    lnkLogout.Click();
                else
                    return false;
            }
            catch (Exception) { }

            Console.WriteLine("Logging Out of BackOffice");
            return (new Login()) != null;

        }

        /// <summary>
        /// Clicks on the Tab and Menu Items from the Fitnesse test
        /// </summary>
        /// <param name="tabname">The name of the Tab from the top level Menu Bar</param>
        /// <param name="topMenu">The name of the first level menu</param>
        /// <param name="subItem">The name of the second level menu item</param>
        /// <returns><c>true</c>if top level menu item found <c>false</c> otherwise</returns>
        /// <by>Deborah DeVine</by>
        /// <created>06/30/2014</created>
        public Boolean ClickOnTabAndSelectTopMenuAndSubMenu(string tabname, string topMenu, string subItem) {


            IWebElement tab = driver.FindLink(tabname);

            if (tab != null)
                tab.SafeClick();
           
            return (new Menu(topMenu, subItem)) != null;


        }

        /// <summary>
        /// Same as the ClickOnTabAndSelectTopMenuAndSubMenu() function, but does not click on tab
        /// </summary>
        /// <param name="tabname">The name of the Tab from the top level Menu Bar</param>
        /// <param name="topMenu">The name of the first level menu</param>
        /// <param name="subItem">The name of the second level menu item</param>
        /// <returns><c>true</c>if top level menu item found <c>false</c> otherwise</returns>
        /// <by>Deborah DeVine</by>
        /// <created>06/30/2014</created>
        public Boolean SelectTopMenuAndSubMenu(string topMenu, string subItem)
        {

            return (new Menu(topMenu, subItem)) != null;
        }



        /// <summary>
        /// Checks that the specified image is found with the src tag
        /// </summary>
        /// <param name="data">location of the image file</param>
        /// <by>Deborah DeVine</by>
        /// <created>06/30/2014</created>

        public Boolean CheckforImage(string logo="../Images/SmallLogo.jpg")
        {
            if (!logo.Equals(""))
            {
                Console.WriteLine("Validating Logo Image with src {1}", logo);


                var pageLogo = driver.FindOne(logo);
                //assert.IsNotNull(pageLogo, "Could not find {0} menu item element.".FormatWith(data.PageLogo));

                if (pageLogo != null)

                    return true;

            }

            return false;
        }

    }

}
