﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
    class SecretPage : fitlibrary.DoFixture
    {


        IWebDriver driver;
       
        [FindsBy(How=How.CssSelector, Using="select[id*='_ddl_Question']")]
        protected IWebElement ddlSecretType;

        [FindsBy(How = How.CssSelector, Using = "input[id*='txtAnswer']")]
        protected IWebElement txtAnswer;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_btnChangeQuestionAnswer']")]
        protected IWebElement changeQuestionButton;

        
        public SecretPage()
        {
            driver = Test.WebDriver;
            //initialize the page objects
            PageFactory.InitElements(driver, this);

            //wait until change question button displays

            driver.Wait(d => (d.PageRefreshed() && changeQuestionButton.IsVisible()), GlobalTestProperties.timeout, GlobalTestProperties.Wait);

           if (changeQuestionButton == null)
               throw new NoSuchWindowException("Change Secret Password Screen Not Displayed");


        }

        public Boolean ChangeSecretQuestion(string secretType, string secret)
        {
            
            //select the secret question type 
            ddlSecretType.SelectValue(secretType);
            txtAnswer.ClearAndSendKeys(secret);

            //click on the change password button
            changeQuestionButton.Click();

            //should return to the main page
            return ((new MainPage()) != null);
        }

    }
}
