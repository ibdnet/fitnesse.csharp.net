﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp {

    public class CustomerLocator 
    {
        private IWebDriver driver;


        [FindsBy(How = How.CssSelector, Using = "input[id*='accountSearch_Searchtxt']")]
        private IWebElement txtCompanyName;

        [FindsBy(How = How.CssSelector, Using = "input[id*='accountSearch_AccountSearchButton']")]
        private IWebElement btnCompanySearch;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_accountSearch_SearchType']")]
        private IWebElement ddlSearchType;


        [FindsBy(How = How.CssSelector, Using = "input[id*='_btnLocateCustomer']")]
        private IWebElement btnLocateCustomer;

        
        //private string companyName;
        //[FindsBy(How=How.CssSelector, Using="input[id*='uc_AddCustomer_pnl_Close']")]
        // protected IWebElement btnClose;

        //Search strings for dynamic webpage elements
        //protected string	selectParentPopup = "div[id*='uc_AddCustomer_pnl_SelectNewParent']";
        //protected string  waitImageSelector = "div[id*='uc_Devices_upprogDevices']";
        public string companyName;
        public string searchBy;
        public string searchText;

        public CustomerLocator(string _companyName, string _searchBy, string _searchText)
        {
            driver = Test.WebDriver;
            //initialize the page objects
            PageFactory.InitElements(driver, this);

            if (btnLocateCustomer != null)
                driver.PerformActionAndWait(() => btnLocateCustomer.Click(), (currentDriver) =>
                    {
                        System.Threading.Thread.Sleep(3000);
                        return !currentDriver.IsVisible(GlobalTestProperties.ModalWaitWindow) && btnCompanySearch != null;
                    });
            else
                throw new NotFoundException("Locate Customer Button Not Found");
            
            //initialiaze shared fitnesse variables
            companyName = _companyName;
            searchBy = _searchBy;
            searchText = _searchText;

        }


        public CustomerLocator()
        {
            driver = Test.WebDriver;
            //initialize the page objects
            PageFactory.InitElements(driver, this);

            if (btnLocateCustomer != null)
                driver.PerformActionAndWait(() => btnLocateCustomer.Click(), (currentDriver) => !currentDriver.IsVisible(GlobalTestProperties.ModalWaitWindow) && btnCompanySearch != null);
            else
                throw new NotFoundException("Locate Customer Button Not Found");

        }

        public Boolean LocateCompany()
        {

            if (btnCompanySearch != null)
            {
                if (txtCompanyName == null)
                    return false;
                else
                {
                    this.driver.PerformActionAndWait(() =>ddlSearchType.SelectValue(searchBy),(currentDriver)=>currentDriver.PageRefreshed());

                   // Console.WriteLine("Selecting  Company {0}", companyName);
                    txtCompanyName.ClearAndSendKeys(searchText);

                    driver.PerformActionAndWait(() =>
                    btnCompanySearch.SafeClick(), (currentDriver) => currentDriver.FindLink(companyName) != null);


                    driver.PerformActionAndWait(() =>
                           driver.FindLink(companyName).Click(),

                           (currentDriver) => btnLocateCustomer.IsVisible());

                    return true;
                }
            }
            return false;
        }

    }
  
}
