﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;


namespace FitNesseCSharp
{
    public class MerchantExstension : MerchantBase
    {


        public MerchantExstension()
            {
                driver = Test.WebDriver;
                PageFactory.InitElements(driver, this);

                if (ddlProcessor == null)
                    throw new NoSuchWindowException("Merchant Page Not Displayed");
                else
                    Console.WriteLine("Merchant Processor Page");
            }

        /// <summary>
        /// Fills the add merchant info.
        /// </summary>
        /// <param name="this.driver">The current driver.</param>
        /// <param name="testData">The test data.</param>
        /// <by>Chris Belford</by>
        /// <created>11/7/2012</created>
        public  string AddPNIProcessor()
        {
           // AddEditMerchantPageTestData addEditMerchPageTestData = new AddEditMerchantPageTestData();
            //AddMerchantValuesTestData addMerchantValues = new AddMerchantValuesTestData();
            if (ddlProcessor != null)
            {
                //Choose processor and wait for the modal wait window to go away and for the screen to be in a "ready" state (determined by checking for
                //visibility of currency drop down selector)
                this.driver.PerformActionAndWait(
                    () => ddlProcessor.SelectValue(processor),
                    (idriver) => idriver.PageRefreshed()
                        && ddlCurrency.IsVisible());

                //Set friendly name
                if (!friendlyName.Equals(string.Empty))
                {
                    friendlyName = CommonExtensions.GenerateRandomID(friendlyName);
                    input_friendlyName.ClearAndSendKeys(friendlyName);
                }

                //Set merchant number
                if (!merchantNumber.Equals(string.Empty))
                {
                    merchantNumber = CommonExtensions.GenerateRandomID(merchantNumber);
                    input_MerchantNumber.ClearAndSendKeys(merchantNumber);
                }

                //Set industry code and wait for modal wait window to disappear and country drop down selector to populate/have the option which will be selected
                //in the next step
                if (!country.Equals(string.Empty))
                {
                    this.driver.PerformActionAndWait(
                        () => ddlIndustryCode.SelectValue(industryCode),
                        (idriver) => idriver.PageRefreshed()
                            && ddlCountry.DropDownHasElementWithDisplayText(country));
                }

                //Set the country and wait for modal wait window to disappear and currency drop down selector to populate/have the option which will be selected/
                //in the next step
                if (!currency.Equals(string.Empty))
                {
                    this.driver.PerformActionAndWait(
                        () => ddlCountry.SelectValue(country),
                        (idriver) => idriver.PageRefreshed()
                            && !updateprogress.IsVisible());
                }

                //Select the currency
                if (!currency.Equals(string.Empty))
                {
                    this.driver.PerformActionAndWait(() =>
                       ddlCurrency.SelectValue(currency), (idriver) => idriver.PageRefreshed() && !updateprogress.IsVisible());
                }

                //Set the sthis.driver.oft descriptor
                if (!descriptor.Equals(string.Empty))
                    input_softDesc.ClearAndSendKeys(descriptor);


                // this.driver.FindOne(addEditMerchPageTestData.SoftDescriptorInputSelector).SendKeys(Keys.Tab); //send the TAB key
                //issue with Firefox : 050613(dld)
                // if (testData.Browser.Type == Entities.BrowserType.FireFox)

                //Find and click the "select all" link for card types, and wait until all of the checkboxes are checked
                ClickSelectAllCardTypesLink();

                //Uncheck the specified card type from the test data
                ToggleCardTypeCheckboxValue();

                //Set next step value to "Return to Merchants & Terminals"
                //this.driver.PerformActionAndWait(() =>
                ddlNextStep.SelectValue(nextStep); //, (driver) => driver.PageRefreshed()); //1 == "Return to Merchants & Terminals

                //Click save and wait to be redirected to merch & terms screen
                try
                {
                    this.driver.PerformActionAndWait(
                        () => btn_Save.SafeClick(),
                        (idriver) => idriver.PageRefreshed()
                            && !updateprogress.IsVisible());
                }
                catch (WebDriverTimeoutException e)
                {

                    Console.WriteLine(e);
                    driver.StopThis();
                    return null;
                }
            }

            return friendlyName;
        }



        /// <summary>
        /// Clicks the select all card types link.
        /// </summary>
        /// <param name="currentDriver">The current driver.</param>
        /// <param name="addEditMerchPageTestData">The add edit merch page test data.</param>
        /// <by>Chris Belford</by>
        /// <created>11/8/2012</created>
        private  void ClickSelectAllCardTypesLink()
        {
            //Find and click the "select all" link for card types, and wait until all of the checkboxes are checked
           // IWebElement cardTypesPanel = this.FindOne(addEditMerchPageTestData.CardTypesPanelSelector);
            //Fix firefox issue - 05/06/13 (dld)

            WebDriverExtensions.SeleniumExceptionRetry(() =>
            {
                new OpenQA.Selenium.Interactions.Actions(this.driver)
                    .MoveToElement(cardTypesPanel)
                    .Build()
                    .Perform();
            });

            if (cardTypesPanel != null)
            {

                //IWebElement selectAllLink = cardTypesPanel.FindAll("a").First(a => a.Text == addEditMerchPageTestData.SelectAllLinkText);

                Func<IWebDriver, bool> waitUntilAllCheckBoxesChecked = (driver) =>
                {
                    bool uncheckedBoxesExist = GetCardTypeCheckboxes().Any(x => (x.GetAttribute("checked") != "true"));
                    return !uncheckedBoxesExist;
                };

                this.driver.PerformActionAndWait(
                    () => lnkSelectAll.SafeClick(),
                    waitUntilAllCheckBoxesChecked);
            }

            //Assert.IsNotNull(cardTypesPanel, "Unable to locate \"{0}\"".FormatWith(addEditMerchPageTestData.CardTypesPanelSelector));
        }


        /// <summary>
        /// Toggles the card type checkbox value associated with the label whose text is provided.
        /// </summary>
        /// <param name="testData">The test data.</param>
        /// <param name="currentDriver">The current driver.</param>
        /// <param name="checkboxLabelDisplayText">The checkbox label display text.</param>
        /// <by>Chris Belford</by>
        /// <created>11/8/2012</created>
        private void ToggleCardTypeCheckboxValue()
        {
            //AddEditMerchantPageTestData addEditMerchPageTestData = new AddEditMerchantPageTestData();

           
            string[] cardsToUnCheck = cardTypes.Split(',');

            foreach (string cardType in cardsToUnCheck)
            {

                IWebElement checkboxToToggle = this.driver.GetInputFromLabelText(cardTypesPanelSelector, cardType);
                this.driver.PerformActionAndWait(
                    () => checkboxToToggle.SafeClick(),
                    (driver) => driver.PageRefreshed() && !updateprogress.IsVisible());
            }
        }

        private IEnumerable<IWebElement> GetCardTypeCheckboxes()
        {
            
            return cardTypesPanel.FindAll("input").Where(x => x.GetAttribute("type") == "checkbox");
        }


    }
}
