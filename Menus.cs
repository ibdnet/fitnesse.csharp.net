﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;
using System.Linq.Expressions;
namespace FitNesseCSharp
{
    
   
    public  class Menu : fitlibrary.DoFixture
    {

        protected IWebDriver driver;
      
        protected string menuSearchText; 
        
       

        public Menu(string topMenu, string subItem)
        {
            driver = Test.WebDriver;

            menuSearchText = "xpath://div[@class='menuBar']/a[.='" + topMenu + "']";
          
            this.driver.Wait(d => (d.FindOne(menuSearchText) != null), GlobalTestProperties.timeout, GlobalTestProperties.Wait);

            IWebElement menuLink = this.driver.FindOne(menuSearchText);

            if (menuLink == null)
                throw new NoSuchWindowException("Menu Item Not Found");
            else
                Console.WriteLine("Selecting Menu {0} / {1}", topMenu, subItem);
                    
            SelectItem(menuLink, subItem);
                
        }
        
        /// <summary>
        /// Clicks on the top level MenuItem Item, followed by the SubMenu
        /// </summary>
        /// <param name="menuLink">The top menu WebElement</param>
        /// <param name="subItem">The text of the sub menu item</param>
        /// <by>Deborah DeVine</by>
        /// <Created>06/30/2014</Created>
        protected void SelectItem(IWebElement menuLink, string subItem)
        {



            //click the specified menu
            menuLink.SafeClick();   
             string menuItemSearchText = "xpath://div[@class='{0}']/a[.='{1}']".FormatWith("menu",subItem);
                                                                                
             this.driver.Wait(d => d.FindOne(menuItemSearchText) != null, 10, 1);

            //move the cursor to the bottom of the menu item element 
            Actions builder = new Actions(this.driver);
            builder.MoveToElement(this.driver.FindOne(menuSearchText), this.driver.FindOne(menuSearchText).Size.Width - 1,
                                  this.driver.FindOne(menuSearchText).Size.Height - 1);
            var moveToLowerLeft = builder.Build();
            moveToLowerLeft.Perform();
           
            //find and click the specified menu item
            new Actions(this.driver)
                .MoveToElement(menuLink, 20, 0)
                .MoveToElement(this.driver.FindOne(menuItemSearchText), 5, 5)
                .Click()
                .Build()
                .Perform();

            Console.WriteLine("Selected Menu Item: {0}", subItem);
        }

       
       

        
    }
}

    

