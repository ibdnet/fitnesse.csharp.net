﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
  
    public class Reports : ColumnFixture
    {

       
       // private static string reportDir = @"C:\selenium_server\FitNesseRoot\files\testData\report_files";
        private ReportBaseExtension newreport = new ReportBaseExtension();
        //private GetReportLink newlink = new GetReportLink();
        private static string reportDir = @"http://qaietest10/files/testData/report_files";
        
        public override object GetTargetObject()
        {
            return newreport;
        }

        

        public class ReportBaseExtension : ReportBase
        {
            private string _link;
            private string downloadFile { get; set; }
            private FileWatch FileEvent;
            public ReportBaseExtension ()
            {

                driver = Test.WebDriver;

                PageFactory.InitElements(driver, this);

                //load the report page objects

            }

            public string GetLink () {

                int i = 0;
                do
                {
                    System.Threading.Thread.Sleep(1000);
                    i += 1;

                    if (i > GlobalTestProperties.timeout)
                        break;
                } while (_link == null);
                
                //Console.WriteLine("The link is ready: {0}", _link);
                return _link;
            
            }

            public Boolean  RunReport()
            {
               
                
                reportName = reportName.Replace(" ", "").ToLower();

               
                try
                {
                    if (btnLocateCustomer != null)
                    {

                        ReportData.LoadReportPageData(reportName);
                        if (searchBy.Equals("Company Name"))
                            searchText = searchText = companyName;

                        if (!new CustomerLocator(companyName, searchBy, searchText).LocateCompany())
                        {
                            throw new NotFoundException("Unable to search by {0}".FormatWith(searchText));

                        }

                    }
                }
                catch (Exception ) { }

                //if (GenerateReport("?BeginDate=" + DateTime.Now.ToString("MMDDYYYY") + "?EndDate=" + DateTime.Now.ToString("MMDDYYYY")
                //+ "?TransactionType=All"))
                if (GenerateReport())
                {
                    DateTime currentTime = DateTime.Now;
                    System.Threading.Thread.Sleep(1000);
                    

                    if (lnkDownloadExcelFile != null)
                    {
                        
                        try
                        {

                            FileEvent = new FileWatch(currentTime, downloadDir, "*.xls*");
                            
                            FileEvent.FileWatchEvent += new FileWatchEventHandler(OnFileChange);
                            lnkDownloadExcelFile.Click();
                            System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ThreadStart(FileEvent.MonitorFile));
                            //Start the thread.
                            thrd.Start();
                            //GetCommonExtensions.GetLastFileSaved(currentTime, downloadDir, "*.xls*").Replace("[", "%5B").Replace("]", "%5D");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }             

                    }


                   
       
                }
                //else
                 //_link= "No Results Found";

                //return a hyperlink to the saved report

                return true;

            }// end of Open Batch Report


            private  void OnFileChange(object Sender, FileWatchEventArgs e)
            {

                // e will be empty in this sample because we are not passing args.
                
                downloadFile = e.GetFileName.Replace("[", "%5B").Replace("]", "%5D");
                if (downloadFile != null)
                {
                    Console.WriteLine(DateTime.Now.ToString() + ":  {0} created.", downloadFile);

                    if (downloadFile.Equals(string.Empty))
                        _link = "No Results Found";
                    else
                       _link = ("<a href=" + reportDir + ">" + reportName + "</a>");
                }
                else
                    _link = "No Results Found";

                
            }

        }

        

      
    } //End of Report class
}
