﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;


namespace fitnesse.common
{
    /// <summary>
    /// Interface for the IWebDriver
    /// </summary>
    public interface IDriverFactory
    {
        IWebDriver CreateDriver(string browser, string version, string platform);
        
    
    }
}
