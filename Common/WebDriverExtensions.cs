﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.Text.RegularExpressions;




namespace fitnesse.common
{
	/// <summary>
	/// WebDriverExtensions
	/// </summary>
	/// <by>Dan Turco</by>
	///   <created>7/23/2012</created>
	public static class WebDriverExtensions
	{
		#region --Public Static Members--


        public static bool AjaxCallComplete(this IWebDriver currentDriver)


        {
            string ajaxString = "return jQuery.active";
            string ajaxState = "";
            SeleniumExceptionRetry(() =>
               {
                   ajaxState = ((IJavaScriptExecutor)currentDriver).ExecuteScript(ajaxString).ToString();
                           
               });


            Thread.Sleep(1000);
            return ajaxState.Equals("0");


        }
        public static bool PageRefreshed(this IWebDriver currentDriver)
        {

            string jString = "return document.readyState";
            string complete = "complete";

            string readyState = "";
            SeleniumExceptionRetry (()=>
            currentDriver.Wait((driver) =>
            {
                readyState = driver.ExecuteJs<string>(jString);
                return readyState.Equals(complete);
            }, 30, 5));

            

            return (readyState.Equals(complete));
           // return (ajaxState.Equals("0"));
           
        }



        public static bool IsAlertPresent(this IWebDriver currentDriver)
        {
            try
            {
                currentDriver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        public static string ConfirmAlertDialog(this IWebDriver currentDriver, bool confirm)
        {

            string alertText = "";

            try
            {
                IAlert alert = currentDriver.SwitchTo().Alert();
                if (alert != null)
                    alertText = alert.Text;
                if (confirm)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                //return alertText;
            }
            catch (Exception) { }
            //finally
            //{
            //   
            //}

            return alertText;
        }

        /// <summary>
        /// returns the line of a element with text separated by the delimeter specified
        /// </summary>
        /// <param name="element">The element with text</param>
        /// <param name="line">the line to return</param>
        /// <returns>the text at the line specified if found, empty string otherwise</returns>
        public static string GetLine(this IWebElement element, int line, string delimeter)
        {
            string[] lines = element.Text.Split(new[] { delimeter }, StringSplitOptions.None);

            if (lines.Length >=line ) 
               return lines[line].Split(':')[1].Trim();

            return string.Empty;

        }
        public static IWebElement FindLink(this IWebDriver currentDriver, By by)
        {
            //trace.TraceInformation("FindOneLink({0})".FormatWith(linkText));
            try
            {     
                var elem = currentDriver.FindElement(by);
                return elem;
            }
            catch (NoSuchElementException)
            {
                return null;
            }
        }
		/// <summary>
		/// Clears the specified search context.
		/// </summary>
		/// <param name="element">Element</param>
		/// <param name="keys">Keys</param>
		/// <by>Dan Turco</by>
		/// <created>7/23/2012</created>
		public static void ClearAndSendKeys(this IWebElement element, string keys)
		{

           
            if (element != null)
            {     
                element.Clear();

               
                element.SendKeys(keys);
            }
            else
                throw new NotFoundException("Element not found");

		
		}

		/// <summary>
		/// Clears the element and sends keystrokes to the element.
		/// </summary>
		/// <param name="searchContext">The search context within which to find the element.</param>
		/// <param name="selector">The selector to select thee element.</param>
		/// <param name="text">The text to send to the element.</param>
		public static void ClearAndSendKeys(this ISearchContext searchContext, string selector, string text)
		{
			//Trace.TraceInformation("ClearAndSendKeys({0}, {1})".FormatWith(selector, text));
            var regexItem = new System.Text.RegularExpressions.Regex("^[{a-zA-Z0-9}]");

			SeleniumExceptionRetry(() =>
			{
			  IWebElement element = searchContext.FindOne(selector);
              if (element != null)
              {
                  
                  element.Clear();

                  Stopwatch swatch = new Stopwatch();

                  swatch.Start();
                  do
                  {

                      element.SendKeys(text);

                      if ((swatch.ElapsedMilliseconds > 20000) || regexItem.IsMatch(text) || text.Trim().Equals(""))                      
                          break;      

                  } while (element.Text.Length == 0);

                  swatch.Stop();
              }
			});
		}

		public static IWebElement Click(this ISearchContext searchContext, string selector)
		{
			//Trace.TraceInformation("Click({0})".FormatWith(selector));

			IWebElement btn = null;

            
            SeleniumExceptionRetry(() =>
               {
                    btn = searchContext.FindOne(selector);
                    btn.Click();
               });
          
			return btn;
		}

		/// <summary>
		/// Executes the js.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="driver">Driver</param>
		/// <param name="javascript">Javascript</param>
		/// <returns>
		///   <see cref="T"/>
		/// </returns>
		/// <by>Dan Turco</by>
		/// <created>7/23/2012</created>
		public static T ExecuteJs<T>(this IWebDriver driver, string javascript)
		{
			//Trace.TraceInformation("ExecuteJs({0})".FormatWith(javascript));
			var js = driver as IJavaScriptExecutor;

			Debug.Assert(js != null);

			return (T)js.ExecuteScript(javascript);
		}

		/// <summary>
		/// Executes the js.
		/// </summary>
		/// <param name="driver">Driver</param>
		/// <param name="javascript">Javascript</param>
		/// <by>Dan Turco</by>
		/// <created>7/23/2012</created>
		public static void ExecuteJs(this IWebDriver driver, string javascript)
		{
			//trace.TraceInformation("ExecuteJs({0})".FormatWith(javascript));
			var js = driver as IJavaScriptExecutor;

			Debug.Assert(js != null);

			js.ExecuteScript(javascript);
		}

		/// <summary>
		/// Executes the js.
		/// </summary>
		/// <param name="driver">Driver</param>
		/// <param name="javascript">Javascript</param>
		/// <param name="args">Args</param>
		/// <by>Dan Turco</by>
		/// <created>7/25/2012</created>
		public static void ExecuteJs(this IWebDriver driver, string javascript, object[] args)
		{
			//trace.TraceInformation("ExecuteJs({0})".FormatWith(javascript));
			var js = driver as IJavaScriptExecutor;
            
			Debug.Assert(js != null);

			js.ExecuteScript(javascript, args);
		}

		/// <summary>
		/// gets the select the options.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <returns>
		///   <see>
		///     <cref>System.Collections.Generic.IList&amp;lt;OpenQA.Selenium.IWebElement&amp;gt;</cref>
		///   </see> </returns>
		/// <by>Dan Turco</by>
		/// <created>7/23/2012</created>
		public static IList<IWebElement> GetSelectOptions(this ISearchContext searchContext, string selector)
		{
			IWebElement context = searchContext.FindOne(selector);
			return new SelectElement(context).Options;
		}

        public static IList<IWebElement> GetSelectOptions(this IWebElement element)
        {
            //IWebElement context = searchContext.FindOne(selector);
            return new SelectElement(element).Options;
        }

		/// <summary>
		/// gets the selected option from the select.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <returns>
		///   <see>
		///     <cref>System.Collections.Generic.IList&amp;lt;OpenQA.Selenium.IWebElement&amp;gt;</cref>
		///   </see> </returns>
		/// <by>Scott Wilfong</by>
		/// <created>9/28/2012</created>
		public static IWebElement GetSelectedOption(this ISearchContext searchContext, string selector)
		{
			IWebElement selectedOption = null;

			SeleniumExceptionRetry(() =>
			{
				var context = searchContext.FindOne(selector);
				var x = new SelectElement(context);
				selectedOption = x.SelectedOption;
			});

			return selectedOption;
		}

        public static IWebElement GetSelectedOption(this IWebElement element)
        {
            IWebElement selectedOption = null;

            SeleniumExceptionRetry(() =>
            {
                //var context = searchContext.FindOne(selector);
                var x = new SelectElement(element);
                selectedOption = x.SelectedOption;
            });

            return selectedOption;
        }

		/// <summary>
		/// Determines whether the specified web driver is visible.
		/// </summary>
		/// <param name="webDriver">Web driver</param>
		/// <param name="jQuerySelector">J query selector</param>
		/// <returns>
		/// "true" if the specified web driver is visible; otherwise, "false".
		/// </returns>
		/// <by>Dan Turco</by>
		/// <created>7/23/2012</created>
		public static bool IsVisibleByJQuery(this IWebDriver webDriver, string jQuerySelector)
		{
			string jsFormat = jQuerySelector.Contains("$") ? "return $({0}).is(\":visible\")" : "return $(\"{0}\").is(\":visible\")";
			string javascript = jsFormat.FormatWith(jQuerySelector);
			var isVisible = webDriver.ExecuteJs<bool>(javascript);

			return isVisible;
		}

		/// <summary>
		/// Determines whether the specified element is visible.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <returns>
		///   <c>true</c> if the specified element is visible; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsVisible(this IWebElement element)
		{
			return IsElementVisible(element);
		}

		/// <summary>
		/// Determines whether [is element visible] [the specified element].
		/// </summary>
		/// <param name="element">The element.</param>
		/// <returns>
		///   <c>true</c> if [is element visible] [the specified element]; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsElementVisible(IWebElement element)
		{
			bool isVisible = false;

			SeleniumExceptionRetry(() => isVisible = ((element != null) && element.Displayed));

            Thread.Sleep(GlobalTestProperties.Wait * 500);
			return isVisible;
		}

		/// <summary>
		/// Determines whether the item specified by the selector on the specified search context is visible.  Will 
		/// throw an exception if no matching element is found.
		/// </summary>
		/// <param name="searchContext">The search context.</param>
		/// <param name="selector">The selector.</param>
		/// <returns>
		///   <c>true</c> if the specified search context is visible; otherwise, <c>false</c>.
		/// </returns>
		/// <by>Chris Belford</by>
		/// <created>10/23/2012</created>
		public static bool IsVisible(this ISearchContext searchContext, string selector)
		{
			IWebElement element = searchContext.FindOne(selector);
			return IsElementVisible(element);
		}

		/// <summary>
		/// Javascripts the click.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <param name="driver">Driver</param>
		/// <returns>
		///   <see cref="OpenQA.Selenium.IWebElement"/>
		/// </returns>
		/// <by>Dan Turco</by>
		/// <created>7/25/2012</created>
		public static IWebElement JavascriptClick(this ISearchContext searchContext, string selector, IWebDriver driver)
		{
			var btn = searchContext.FindOne(selector);
			driver.ExecuteJs("arguments[0].fireEvent('onclick');", new object[] { btn });
			return btn;
		}

		/// <summary>
		/// Performs the provided action and waits until the wait condition is met.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="action">The click action.</param>
		/// <param name="waitCondition">The wait condition.</param>
		/// <param name="secondsToWait">The seconds to wait.</param>
		/// <param name="secondsToSleepBeforeCheckingAgain">The seconds to sleep before checking again.</param>
		/// <by>Chris Belford</by>
		/// <created>10/19/2012</created>
		public static void PerformActionAndWait(this IWebDriver driver, Action action, Func<IWebDriver, bool> waitCondition, int secondsToWait, int secondsToSleepBeforeCheckingAgain)
		{
			if (action == null)
			{
				throw new ArgumentException("No action provided.  Use 'Wait' call instead if no click action is required.");
			}

			if (waitCondition == null)
			{
				throw new ArgumentException("No wait condition provided.");
			}

			SeleniumExceptionRetry(action); //Perform action

			//Instantiate class responsible for waiting
			WebDriverWait wait = new WebDriverWait(new SystemClock(), driver, new TimeSpan(0, 0, 0, secondsToWait), new TimeSpan(0, 0, 0, secondsToSleepBeforeCheckingAgain));

			/* Wait condition is occassionally raising a StaleReferenceException (WebDriverException).  To handle this, we will return false when the 
			 * exception occurs, indicating that the wait condition failed, and it will retry again after the appropriate time.  This will (hopefully)
			 * ensure that the next time the condition is tested a valid reference will be gotten.  This issue only happens intermittently, and most in
			 * Internet Explorer. */
			Func<IWebDriver, bool> safeWaitCondition = d =>
			{
				try
				{
					return waitCondition(d);
				}
				catch (WebDriverException)
				{
					return false;
				}
				catch (InvalidOperationException)
				{
					return false;
				}
			};

			//Wait
			wait.Until(safeWaitCondition);
		}

		/// <summary>
		/// Performs the provided action and waits until the wait condition is met.  Uses defaults from ini file for wait condition, retesting wait condition every 1 second.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="action">The action.</param>
		/// <param name="waitCondition">The wait condition.</param>
		/// <by>Chris Belford</by>
		/// <created>11/5/2012</created>
        /// <modified name="by">Deborah DeVine</modified>
        /// <modified name="date">06/03/2013</modified>
		public static void PerformAndWaitAndRetry(this IWebDriver driver, Action action, Func<IWebDriver, bool> waitCondition)
		{
            PerformAndWaitAndRetry(driver, action, waitCondition, GlobalTestProperties.timeout, 10);
		}

		/// <summary>
		/// Performs the provided action and waits until the wait condition is met.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="action">The click action.</param>
		/// <param name="waitCondition">The wait condition.</param>
		/// <param name="secondsToWait">The seconds to wait.</param>
		/// <param name="secondsToSleepBeforeCheckingAgain">The seconds to sleep before checking again.</param>
		/// <by>Chris Belford</by>
		/// <created>10/19/2012</created>
		public static void PerformAndWaitAndRetry(this IWebDriver driver, Action action, Func<IWebDriver, bool> waitCondition, int secondsToWait, int secondsToSleepBeforeCheckingAgain)
		{
			try
			{
				driver.PerformActionAndWait(action, waitCondition, secondsToWait, secondsToSleepBeforeCheckingAgain);
			}
			catch (Exception)
			{
				//Retry once
                Console.WriteLine("Retrying action again");
                driver.PerformActionAndWait(action, waitCondition, secondsToWait, secondsToSleepBeforeCheckingAgain);
				
			}
		}

		/// <summary>
		/// Selects the option by display text (case sensitive).
		/// </summary>
		/// <param name="searchContext">The search context.</param>
		/// <param name="selector">The selector.</param>
		/// <param name="displayText">The display text.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>11/6/2012</created>
		public static IWebElement SelectOptionByDisplayText(this ISearchContext searchContext, string selector, string displayText)
		{
            //return SelectOptionByDisplayText(searchContext, selector, displayText, () =>
            //{
            //}); //No Wait

            return SelectOptionByDisplayText(searchContext, selector, displayText, null);
		}

		/// <summary>
		/// Selects the option by display text (case sensitive).
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <param name="displayText">Display text</param>
		/// <param name="waitAction">Wait action</param>
		/// <returns>
		///   <see cref="OpenQA.Selenium.IWebElement"/>
		/// </returns>
		/// <by>Dan Turco</by>
		/// <created>7/23/2012</created>
		public static IWebElement SelectOptionByDisplayText(this ISearchContext searchContext, string selector, string displayText, Action waitAction)
		{
           // Console.WriteLine("Selector {0} and displaytext {1}", selector, displayText);

         
			IWebElement selected = searchContext.SelectOption(selector, element =>
			{
				try
				{
                  //  Console.WriteLine(element.Text == displayText ? "Element text {0} equals Display Text {1}" : "Element text {0} DOES NOT equal Display Text {1}", element.Text, displayText);

					return element.Text.Contains(displayText);
				}
				catch (StaleElementReferenceException)
				{
                    return false;
				}
				//return false;
			}, waitAction);

			if (selected == null)
			{
				throw new NotFoundException("Option '{0}' not found for selector '{1}.'".FormatWith(displayText, selector));
			}

			return selected;
		}

		/// <summary>
		/// Selects the option by the value attribute.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <param name="value">Value</param>
		/// <returns>
		///   <see cref="OpenQA.Selenium.IWebElement"/>
		/// </returns>
		/// <by>Scott Wilfong</by>
		/// <created>7/23/2012</created>
		public static IWebElement SelectOptionByValue(this ISearchContext searchContext, string selector, string value)
		{
			return SelectOptionByValue(searchContext, selector, value, null);
		}

		/// <summary>
		/// Selects the option by display text.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <param name="value">Value</param>
		/// <param name="waitAction">Wait action</param>
		/// <returns>
		///   <see cref="OpenQA.Selenium.IWebElement"/>
		/// </returns>
		/// <by>Scott Wilfong</by>
		/// <created>7/23/2012</created>
		public static IWebElement SelectOptionByValue(this ISearchContext searchContext, string selector, string value, Action waitAction)
		{
			IWebElement option = null;
			SeleniumExceptionRetry(() =>
			{
				option = searchContext.SelectOption(selector, element => element.GetAttribute("value") == value, waitAction);
             
			});
			return option;
		}


        
        public static IWebElement SelectOptionByText(this ISearchContext searchContext, string selector, string value)
        {
           
              return  SelectOptionByText(searchContext, selector, value, null);
          
        }


        public static IWebElement SelectOptionByText(this ISearchContext searchContext, string selector, string displayText, Action waitAction)
        {
            // Console.WriteLine("Selector {0} and displaytext {1}", selector, displayText);

            IWebElement option = null;
            SeleniumExceptionRetry(() =>
            {
                option = searchContext.SelectOptionByText(selector, displayText);
            });
            return option;
        }

      
		/// <summary>
		/// Selects the option by it's position in the select list.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <param name="position">Position</param>
		/// <returns>
		///   <see cref="OpenQA.Selenium.IWebElement"/>
		/// </returns>
		/// <by>Scott Wilfong</by>
		/// <created>7/23/2012</created>
		public static IWebElement SelectOptionByPosition(this ISearchContext searchContext, string selector, int position)
		{
			return searchContext.SelectOptionByPosition(selector, position, null);
		}

		/// <summary>
		/// Selects the option by its position in the select list.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <param name="position">Position</param>
		/// <param name="waitAction">Wait action</param>
		/// <returns>
		///   <see cref="OpenQA.Selenium.IWebElement"/>
		/// </returns>
		/// <by>Scott Wilfong</by>
		/// <created>7/23/2012</created>
		public static IWebElement SelectOptionByPosition(this ISearchContext searchContext, string selector, int position, Action waitAction)
		{
			IWebElement option = null;
			SeleniumExceptionRetry(() =>
			{
				option = searchContext.SelectOption(selector, position);
			});
			return option;
		}

        /// <summary>
        /// 
        ///Retries the specified action
        /// </summary>
        /// <param name="action"></param>

		public static void SeleniumExceptionRetry( Action action)
		{
			const int MAX_RETRY_COUNT = 10;
			bool success = false;
			int exceptionCount = 0;

       

			Action<Exception> caughtExceptionHandler = ex =>
			{
				//Swallow exceptions up to exception count max
				exceptionCount++;
                
				if (exceptionCount == MAX_RETRY_COUNT)
				{
					throw ex;
				}

				Thread.Sleep(500); //Putting a thread sleep here allows the appropriate selenium driver to get its act together
			};

			while (!success && (exceptionCount < MAX_RETRY_COUNT))
			{
				try
				{
					action();
					success = true;
				}
				catch (Exception ex)
				{
					caughtExceptionHandler(ex);
				}
             
			}
            
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="action"></param>
        /// <param name="successCondition"></param>

        public static void ActionRetry(this IWebDriver driver, Action action, Func<IWebDriver, bool> successCondition)
        {
            

            WebDriverWait wait = new WebDriverWait(new SystemClock(), driver, new TimeSpan(0, 0, 0, 30), new TimeSpan(0, 0, 0, 5));

                    
            Func<IWebDriver, bool> trueWaitCondition = d  =>
            {
                try
                {
                    action();
                    return successCondition(d);
                }
                catch (WebDriverException)
                {
                    return false;
                }
                catch (InvalidOperationException)
                {
                    return false;
                }
            };

            //Wait
            wait.Until(trueWaitCondition);
        }

		/// <summary>
		/// Types the specified search context.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="selector">Selector</param>
		/// <param name="text">Text</param>
		/// <returns></returns>
		/// <by>Dan Turco</by>
		/// <created>7/23/2012</created>
		public static IWebElement Type(this ISearchContext searchContext, string selector, string text)
		{
			//trace.TraceInformation("Type({0}, {1})".FormatWith(selector, text));

            var element = searchContext.FindElement(By.CssSelector(selector));


            if (element != null)
                  element.SendKeys(text);
           

			return element;
		}


        public static void SetDataValue(this IWebDriver driver , string selector, string text) {

            IWebElement element = driver.FindOne(selector);
		       
         
            Func<string, string> matchType =  d => {

              // string INPUT_DATA =  @"^<[input]+>$";
               string SELECT_DATA = @"^<[select]+>$";
            
               //if ( Regex.Match(d, INPUT_DATA, RegexOptions.IgnoreCase).Success) 
                 //  return "input";
              if ( Regex.Match(d, SELECT_DATA, RegexOptions.IgnoreCase).Success) 
                   return "select";
              

                return "input";
            };


            string selecttype = matchType(selector);

            switch  (selecttype) {

                case "select":
                    element = driver.FindOne(selector);
                    
                    
                    break;

                default :
                    char c = '\u0001'; // ASCII code 1 for Ctrl-A
                    new Actions(driver).MoveToElement(element).Click().Perform();
                    element.SendKeys(Convert.ToString(c));
                    element.SendKeys(text);
                    break;



            }

            //return element;
        }

       
		/// <summary>
		/// Waits until the specificed wait condition evalutes to true.  Defaults to wait 30 seconds, rechecking condition every 1 second(s).
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="waitCondition">The wait condition.</param>
		/// <by>Chris Belford</by>
		/// <created>11/9/2012</created>
		public static void Wait(this IWebDriver driver, Func<IWebDriver, bool> waitCondition)
		{
			Wait(driver, waitCondition, 30, 1);
		}

		/// <summary>
		/// Waits the specified driver.
		/// </summary>
		/// <param name="driver">Driver</param>
		/// <param name="waitCondition">Wait condition</param>
		/// <param name="secondsToWait">Seconds to wait</param>
		/// <param name="secondsToSleepBeforeCheckingAgain">Seconds to sleep before checking again</param>
		/// <by>Dan Turco</by>
		/// <created>7/23/2012</created>
		public static void Wait(this IWebDriver driver, Func<IWebDriver, bool> waitCondition, int secondsToWait, int secondsToSleepBeforeCheckingAgain)
		{
            //Console.WriteLine("Waiting for Page Refresh");
			var wait = new WebDriverWait(new SystemClock(), driver, new TimeSpan(0, 0, 0, secondsToWait), new TimeSpan(0, 0, 0, secondsToSleepBeforeCheckingAgain));
                     
			wait.Until(waitCondition);
		}


        
		/// <summary>
		/// Determines whether [has CSS class] the specified class name.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="className">Name of the class.</param>
		/// <returns>
		///   <c>true</c> if has the specified class name; otherwise, <c>false</c>.
		/// </returns>
		/// <by>Chris Belford</by>
		/// <created>10/29/2012</created>
		public static bool HasCssClass(this IWebElement element, string className)
		{
            bool bHasCssClass = false;

            className = className.TrimStart('.');
            if (className != null)
                bHasCssClass = element.GetAttribute("class").Contains(className);

            return bHasCssClass;
		}

		/// <summary>
		/// Attempts to find an element matching the provided selector, and determines if that element has the provided CSS class.  Will
		/// throw an exception if element does not exist.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="selector">The selector.</param>
		/// <param name="className">Name of the class.</param>
		/// <returns>
		///   <c>true</c> if [has CSS class] [the specified driver]; otherwise, <c>false</c>.
		/// </returns>
		/// <by>Chris Belford</by>
		/// <created>10/29/2012</created>
		/// <exception cref="NullReferenceException">Will throw null reference exception if no element is found matching the provided selector.</exception>
		public static bool HasCssClass(this IWebDriver driver, string selector, string className)
		{

            bool bHasCssClass = false;

            IWebElement element = driver.FindOne(selector);
            bHasCssClass = element.HasCssClass(className);

            return  bHasCssClass;
		}

		/// <summary>
		/// Navigates to relative URL if not already on that page.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="baseUrl">The base URL.</param>
		/// <param name="relativeUrl">The relative URL.</param>
		/// <by>Chris Belford</by>
		/// <created>10/30/2012</created>
		public static void NavigateToRelativeUrl(this IWebDriver driver, string baseUrl, string relativeUrl)
        {

            string targetUrl = baseUrl.PathCombineRelativeURI(relativeUrl);
            
            if (!string.Equals(driver.Url, targetUrl, StringComparison.CurrentCultureIgnoreCase))
        
                driver.Navigate().GoToUrl(targetUrl);
		
		}

		/// <summary>
		/// Wrapper click method which will call the provided element's Click method, but with exception retry logic.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <by>Chris Belford</by>
		/// <created>10/31/2012</created>
		public static void SafeClick(this IWebElement element)
		{

            SeleniumExceptionRetry(element.Click);
		}

		/// <summary>
		/// Performs the provided action and waits until the wait condition is met.  By default will wait 30 seconds, re-checking the wait condition
		/// every second.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="action">The action.</param>
		/// <param name="waitCondition">The wait condition.</param>
		/// <by>Chris Belford</by>
		/// <created>10/31/2012</created>
		public static void PerformActionAndWait(this IWebDriver driver, Action action, Func<IWebDriver, bool> waitCondition)
		{
            PerformActionAndWait(driver, action, waitCondition, GlobalTestProperties.timeout, 3);
		}

		/// <summary>
		/// Returns true when element is found in the DOM, otherwise false.
		/// </summary>
		/// <param name="driver"></param>
		/// <param name="selector"></param>
		/// <returns></returns>
		public static bool ElementExists(this IWebDriver driver, string selector)
		{
			return (driver.FindOne(selector) != null);
		}

		/// <summary>
		/// Returns true if drop down matching provided selector has an option with the provided value, otherwise false.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="dropDownSelector">The drop down selector.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>11/5/2012</created>
        /// <Modified by>Deborah DeVine</Modified>
        /// <Modified Date>07/02/2014</Modified>
        /// <Modified Desc>Modified to extend IWebElement instead of IWebDriver</Modified>
		public static bool DropDownHasElementWithValue(this IWebElement element, string value)
		{
            SelectElement selectList = new SelectElement(element);
            IList<IWebElement> options = selectList.Options;
            
			return (options.FirstOrDefault(x => x.GetAttribute("value") == value) != null);
		}

		/// <summary>
		/// Returns true if drop down matching provided selector has an option with the provided display text, otherwise fales.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="dropDownSelector">The drop down selector.</param>
		/// <param name="displayText">The display text.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>11/8/2012</created>
		public static bool DropDownHasElementWithDisplayText(this IWebDriver driver, string dropDownSelector, string displayText)
		{
            IWebElement element;
            
            element = driver.GetSelectOptions(dropDownSelector).FirstOrDefault(x =>
                {
                    try
                    {
                        return x.Text.Equals(displayText);
                    }
                    catch (StaleElementReferenceException)
                    {
                        return false;
                    }
                });
            return (element!= null );

            
		}


        public static bool DropDownHasElementWithDisplayText(this IWebElement element,  string displayText)
        {
            //IWebElement element;
            SelectElement element2 = new SelectElement(element);

           return  element2.Options.FirstOrDefault(x =>
            {
                try
                {
                    return x.Text.Equals(displayText);
                }
                catch (StaleElementReferenceException)
                {
                    return false;
                }
            }) != null;
           // return (element != null);


        }

		/// <summary>
		/// Gets the value of the "value" attribute for the element matching the provided selector
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="selector">The selector.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>11/8/2012</created>
		public static string GetValueAttribute(this IWebDriver driver, string selector)
		{
			IWebElement element = driver.FindOne(selector);
			return element != null ? element.GetValueAttribute() : "";
		}

		/// <summary>
		/// Gets the value of the "value" attribute of the element
		/// </summary>
		/// <param name="element">The element.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>11/8/2012</created>
		public static string GetValueAttribute(this IWebElement element)
		{
			string value = null;

			SeleniumExceptionRetry(() => value = element.GetAttribute("value"));

			return value;
		}

		/// <summary>
		/// Finds the input in provided container element matching the provided container selector for which there is a label whose text matches wait provided label text.
		/// </summary>
		/// <param name="driver">The driver.</param>
		/// <param name="containerSelector">The container selector.</param>
		/// <param name="labelText">The label text.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>11/9/2012</created>
		public static IWebElement GetInputFromLabelText(this IWebDriver driver, string containerSelector, string labelText)
		{
			IWebElement input = null;

			IWebElement container = driver.FindOne(containerSelector);
			if (container != null)
			{
				input = container.GetInputFromLabelText(labelText);
			}

			return input;
		}

		/// <summary>
		/// Finds the input in the provided container element for which there is a label whose text matches the provided label text.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="labelText">The label text.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>11/9/2012</created>
		public static IWebElement GetInputFromLabelText(this IWebElement container, string labelText)
		{
			IWebElement input = null;
            IWebElement label = null;
           
            SeleniumExceptionRetry(()=> label = container.FindAll("label").FirstOrDefault(x => x.Text == labelText));
			
			if (label != null)
			{
				string idOfInput = label.GetAttribute("for");
				if (idOfInput != null)
				{
					input = container.FindOne("input[id*='{0}']".FormatWith(idOfInput));
				}
			}

			return input;
		}

        /// <summary>
        /// Validates that the image element argument passed has the expected source
        /// </summary>
        /// <param name="element">Image element</param>
        /// <param name="imageSource">tThe expected source string..ie /img/file/test.png</param>
        /// <returns>true if image string found</returns>
        /// <by>Deborah Devine</by>
        /// <created>05/02/2013</created>
        public static bool VerifyImageSource (IWebElement element, string imageSource)
        {
          
            return element.GetAttribute("src").EndsWith(imageSource);


        }


        /// <summary>
        /// Checks that the specified image is found with the src tag
        /// </summary>
        /// <param name="currentDriver">current IWebDriver instance</param>
        /// <param name="data">data from the Menu xml file</param>
        /// <by>Deborah DeVine</by>
        /// <created>05/02/13</created>

        public static string  WarningMessage (this IWebDriver currentDriver)
        {
               
            
            IWebElement warning = currentDriver.FindOne("img[src*='caution.gif']");


            if (warning != null)
            {
                IWebElement message = currentDriver.FindOne("span[style*='color:Red;font-weight:bold;']");
                return message.Text;
            }
            return "";
            


        }

        public static void TakeScreenShot(this IWebDriver driver, string path, string name)
        {
            //rename the file name if it already exists
            string filename = CommonExtensions.GetNextFileName(path, name);
            
            ITakesScreenshot screenshotDriver = driver as ITakesScreenshot;
            try
            {
                Screenshot screenshot = screenshotDriver.GetScreenshot();
                screenshot.SaveAsFile(filename + ".Png", System.Drawing.Imaging.ImageFormat.Png);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                
            }
            

        }

        public static void StopThis (this IWebDriver driver) {

            ((IJavaScriptExecutor)driver).ExecuteAsyncScript("window.stop();");

        }

		#endregion --Public Static Members--

		#region --Private Static Members--

		/// <summary>
		/// Selects the option.
		/// </summary>
		/// <param name="searchContext">The search context.</param>
		/// <param name="selector">The selector.</param>
		/// <param name="evaluator">The evaluator.</param>
		/// <param name="waitAction">The wait action.</param>
		/// <returns></returns>
		/// <exception cref="OpenQA.Selenium.NoSuchElementException">Select element not found with the selector {0}..FormatWith(selector)</exception>
		private static IWebElement SelectOption(this ISearchContext searchContext, string selector, Func<IWebElement, bool> evaluator, Action waitAction)
		{
			//trace.TraceInformation("SelectOption({0})".FormatWith(selector));

			IWebElement selectedOption = null;

            
			IWebElement select = searchContext.FindOne(selector);
            //escape any special characters
            
			if (select == null)
				throw new NoSuchElementException("Select element not found with the selector {0}.".FormatWith(selector));

			ReadOnlyCollection<IWebElement> options = select.FindAll("<option>");

			if (options == null)
				throw new NoSuchElementException("Select element with the selector {0} is missing <option> tags.".FormatWith(selector));
        
			foreach (IWebElement option in options)
			{
               
				if (evaluator(option))
				{
					selectedOption = option;
					SeleniumExceptionRetry(option.Click);

                    if (waitAction != null)
                    {
                        
                        waitAction();
                    }
                    else
                        break; //break out of loop --5.29.2013.1139.dld
                   
				}
			}

			return selectedOption;
		}

        /// <summary>
        /// Using a fastor method of selecting from a list with the SelectElement class object
        /// </summary>
        /// <param name="ddlList"><c>IWebElement</c> for the dropdown list</param>
        /// <param name="value">The value to select</param>
        public static void SelectValue(this IWebElement ddlList, string value)
        {

            SelectElement select = new SelectElement(ddlList);
            //select.DeselectAll();
        
            select.SelectByText(value);

        }
		/// <summary>
		/// Selects the option.
		/// </summary>
		/// <param name="searchContext">The search context.</param>
		/// <param name="selector">The selector.</param>
		/// <param name="position">The position.</param>
		/// <returns></returns>
		/// <exception cref="OpenQA.Selenium.NoSuchElementException">Select element not found with the selector {0}..FormatWith(selector)</exception>
		private static IWebElement SelectOption(this ISearchContext searchContext, string selector, int position)
		{
			//trace.TraceInformation("SelectOption({0})".FormatWith(selector));

			IWebElement select = searchContext.FindOne(selector);

			if (select == null)
				throw new NoSuchElementException("Select element not found with the selector {0}.".FormatWith(selector));

			ReadOnlyCollection<IWebElement> options = select.FindAll("<option>");

			if (options == null)
				throw new NoSuchElementException("Select element with the selector {0} is missing <option> tags.".FormatWith(selector));

			if (position >= options.Count)
				throw new NoSuchElementException("Select element with the position of {0} does not exist.".FormatWith(position));

			return options[position];
		}


        /// <summary>
        /// An expectation for checking whether an element is visible.
        /// </summary>
        /// <param name="locator">The locator used to find the element.</param>
        /// <returns>The <see cref="IWebElement"/> once it is located, visible and clickable.</returns>
        public static Boolean ElementIsClickable(this IWebDriver driver, By locator)
        {
                    
                IWebElement element = driver.FindElement(locator);
                return (element != null && element.Displayed && element.Enabled) ;
           
        }


        /// <summary>
        /// An expectation for checking whether an element is visible.
        /// </summary>
        /// <param name="locator">The locator used to find the element.</param>
        /// <returns>The <see cref="IWebElement"/> once it is located, visible and clickable.</returns>
        public static Boolean ElementIsClickable(this IWebElement element)
        {

          
            return (element != null && element.Displayed && element.Enabled);

        }

        public static Boolean FindText (this IWebDriver driver, string text) {
            
            
            //var selenium = new Selenium.WebDriverBackedSelenium(driver, driver.Url);
            
           // return selenium.IsTextPresent(text);
            return driver.FindElement(By.TagName("body")).Text.ToLower().Contains(text.ToLower());
        }
		#endregion --Private Static Members--
	}
}