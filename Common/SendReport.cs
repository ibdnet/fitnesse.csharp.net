﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace fitnesse.common
{
    public static class CompileReport
    {
        public static void SendReport(string reportTitle, string inFile, string outDir, string userID)
        {
   
            //string[] splitDir = tDetails.FullName.Split('.');
            int maxCount = 120;

           
            Console.WriteLine("********Test Suite completed : Generating Report*********");

            Func<string, bool> getFileInfo = file1 => File.Exists(file1) ;

            string curDir = GlobalTestProperties.projectDir; // Directory.GetCurrentDirectory();//.Substring(0, Directory.GetCurrentDirectory().Length) + @"\TestResult.xml";
         
            int count = 0;
            while (!getFileInfo(inFile) )
            {
                count++;
   
                if (count > maxCount)
                {
                    Console.WriteLine("Max retry count reached for attempting to wait for {0}", inFile);
                    return;
                  
                    
                }
                    
            }   
            Console.WriteLine("Result File : {0}", @inFile);
            Console.WriteLine("Executing : {0}", @curDir + "\\nunit-results " + "\"" + @inFile + "\" " + outDir);
            CommonExtensions.ExecuteCommandSync(@curDir+ "\\nunit-results " + "\"" + @inFile + "\" " + outDir);
           // if (GlobalTestProperties.sendmail)
           // {
                Console.WriteLine("Sending Report to {0} ", userID);
                //CommonExtensions.ZipAndEmailReport(reportTitle, userID, outDir);
            //}
        }
    }
        
 }

