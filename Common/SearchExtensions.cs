﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using OpenQA.Selenium;

namespace fitnesse.common
{
	/// <summary>
	/// Extensions used for searching for DOM elements using ISearchContext and custom selector declarations (matching jQuery)
	/// </summary>
	/// <by>Chris Belford</by>
	/// <created>10/19/2012</created>
	public static class SearchExtensions
	{
		#region --Private Static Fields--

		private static readonly Regex HTML_TAG_REGEX = new Regex(@"^<[a-zA-Z]+>$", RegexOptions.Compiled);
		private static readonly Regex LETTERS_ONLY_REGEX = new Regex(@"^[a-zA-Z]+$", RegexOptions.Compiled);
		private static readonly Regex SELECTOR_REGEX = new Regex(@"^([a-zA-Z]*)(.{1})(.+)$", RegexOptions.Compiled);
		private static readonly Regex XPATH_REGEX = new Regex(@"^xpath:(.+)$", RegexOptions.Compiled);

		#endregion --Private Static Fields--

		#region --Public Static Members--

		/// <summary>
		/// Finds all matching elements using the specified search context and the provided selector.
		/// </summary>
		/// <param name="searchContext">The search context.</param>
		/// <param name="selector">The value.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>10/19/2012</created>
		/// <exception cref="System.IO.InvalidDataException"></exception>
		public static ReadOnlyCollection<IWebElement> FindAll(this ISearchContext searchContext, string selector)
		{
			ReadOnlyCollection<IWebElement> elements = null;

			WebDriverExtensions.SeleniumExceptionRetry(delegate
			{
				MatchResult matchResult = DetermineMatchType(selector);

				switch (matchResult.Type)
				{
					case MatchResult.MatchType.Tag:
						elements = FindAllTags(searchContext, matchResult);
						break;
					case MatchResult.MatchType.TagName:
						elements = FindAllTagsByTagName(searchContext, matchResult);
						break;
					case MatchResult.MatchType.XPath:
						elements = FindAllElementsByXPath(searchContext, matchResult);
						break;
					case MatchResult.MatchType.Selector:
						elements = FindAllElementsBySelector(searchContext, matchResult);
						break;
				}
			});

			if (elements == null)
			{
				throw new InvalidDataException(string.Format("Selector '{0}' is an unknown format.", selector));
			}

			return elements;
		}

		/// <summary>
		/// Finds the link whose text matches the provided link text.
		/// </summary>
		/// <param name="searchContext">Search context</param>
		/// <param name="linkText">Link text</param>
		/// <returns>
		///   <see cref="OpenQA.Selenium.IWebElement" />
		/// </returns>
		/// <by>Dan Turco</by>
		/// <created>8/9/2012</created>
		public static IWebElement FindLink(this ISearchContext searchContext, string linkText)
		{
			//trace.TraceInformation("FindOneLink({0})".FormatWith(linkText));
			try
			{
				var elem = searchContext.FindElement(By.LinkText(linkText));
				return elem;
			}
			catch (NoSuchElementException)
			{
				return null;
			}
		}

		/// <summary>
		/// Finds first matching element using the specified search context and the provided selector.
		/// </summary>
		/// <param name="searchContext">The search context.</param>
		/// <param name="selector">The value.</param>
		/// <returns></returns>
		/// <by>Chris Belford</by>
		/// <created>10/19/2012</created>
		public static IWebElement FindOne(this ISearchContext searchContext, string selector)
		{
			//Refactor:  This can be made faster by not relying on FindAll (which uses FindElements instead of FindElement).
			IWebElement found = searchContext.FindAll(selector).FirstOrDefault();
                     
			return found;
		}

        
		#endregion --Public Static Members--

		#region --Private Static Members--

		private static MatchResult DetermineMatchType(string selector)
		{
			//Order is important in the below listing
			List<Tuple<Regex, MatchResult.MatchType>> matchGroupings = new List<Tuple<Regex, MatchResult.MatchType>>
			{
				new Tuple<Regex, MatchResult.MatchType>(HTML_TAG_REGEX, MatchResult.MatchType.Tag),
				new Tuple<Regex, MatchResult.MatchType>(LETTERS_ONLY_REGEX, MatchResult.MatchType.TagName),
				new Tuple<Regex, MatchResult.MatchType>(XPATH_REGEX, MatchResult.MatchType.XPath),
				new Tuple<Regex, MatchResult.MatchType>(SELECTOR_REGEX, MatchResult.MatchType.Selector),
			};

			return (from grouping in matchGroupings let regexMatch = grouping.Item1.Match(selector) where regexMatch.Success select new MatchResult(selector, grouping.Item2, regexMatch)).FirstOrDefault();
		}

		private static ReadOnlyCollection<IWebElement> FindAllElementsBySelector(ISearchContext searchContext, MatchResult matchResult)
		{
			SelectorInfo selectorInfo = new SelectorInfo(matchResult.RegexMatch);

			ReadOnlyCollection<IWebElement> elements = !string.IsNullOrEmpty(selectorInfo.TagType) ? FindAllTagTypeSpecificElements(searchContext, selectorInfo) : FindAllNonTagTypeSpecificElements(searchContext, selectorInfo);

			return elements;
		}

		private static ReadOnlyCollection<IWebElement> FindAllElementsByXPath(ISearchContext searchContext, MatchResult match)
		{
			string xPath = match.RegexMatch.Value.Substring(6);
			return searchContext.FindElements(By.XPath(xPath));
		}

		private static ReadOnlyCollection<IWebElement> FindAllNonTagTypeSpecificElements(ISearchContext searchContext, SelectorInfo selectorInfo)
		{
			ReadOnlyCollection<IWebElement> elements = null;

			switch (selectorInfo.Selector)
			{
				case ".":
					elements = searchContext.FindElements(By.CssSelector("." + selectorInfo.ToMatch));
					break;
				case "#":
					elements = searchContext.FindElements(By.Id(selectorInfo.ToMatch));
					break;
			}

			return elements;
		}

		private static ReadOnlyCollection<IWebElement> FindAllTagTypeSpecificElements(ISearchContext searchContext, SelectorInfo selectorInfo)
		{
			ReadOnlyCollection<IWebElement> elements = null;
			IList<IWebElement> tempResults;

			switch (selectorInfo.Selector)
			{
				case "[": //Attribute matching
					elements = FindAllTagsWithAttribute(searchContext, selectorInfo, elements);
					break;
				case ".": //CSS Class
					tempResults = searchContext.FindElements(By.TagName(selectorInfo.TagType));
					tempResults = tempResults.Where(x => x.GetAttribute("class").Contains(selectorInfo.ToMatch)).ToList();
					elements = new ReadOnlyCollection<IWebElement>(tempResults);
					break;
				case "#": //ID
					tempResults = searchContext.FindElements(By.TagName(selectorInfo.TagType));
					tempResults = tempResults.Where(x => string.Equals(x.GetAttribute("id"), selectorInfo.ToMatch)).ToList();
					elements = new ReadOnlyCollection<IWebElement>(tempResults);
					break;
			}

			return elements;
		}

		private static ReadOnlyCollection<IWebElement> FindAllTags(ISearchContext searchContext, MatchResult match)
		{
			string tagName = match.RegexMatch.Value.TrimStart('<').TrimEnd('>');
			return FindAllTagsByTagName(searchContext, tagName);
		}

		private static ReadOnlyCollection<IWebElement> FindAllTagsByTagName(ISearchContext searchContext, string tagName)
		{
			return searchContext.FindElements(By.TagName(tagName));
		}

		private static ReadOnlyCollection<IWebElement> FindAllTagsByTagName(ISearchContext searchContext, MatchResult match)
		{
			return FindAllTagsByTagName(searchContext, match.RegexMatch.Value);
		}

		private static ReadOnlyCollection<IWebElement> FindAllTagsWithAttribute(ISearchContext searchContext, SelectorInfo selectorInfo, ReadOnlyCollection<IWebElement> elements)
		{
			if (!selectorInfo.ToMatch.EndsWith("]"))
			{
				throw new InvalidDataException("Searching by attribute requires both starting and ending brackets [name^='value'].");
			}

			//Something like input[id$='value'] or input[name='value'] was found.
			string toMatch = selectorInfo.ToMatch.Substring(0, selectorInfo.ToMatch.Length - 1); //Trim "]" from end of "toMatch" string

			By by = null;
			string[] split = toMatch.Split('=');

			if (split.Length > 1)
			{
				//'=' found in selector, parse out lookup type
				Match lookupTypeMatch = LETTERS_ONLY_REGEX.Match(split[0]);
				bool isPartialAttributeValueLookup = !lookupTypeMatch.Success;
				if (isPartialAttributeValueLookup)
				{
					//Something like '^=' or '*=' was found (partial attribute value lookup).  Need to handle the character preceeding the '=' for appropriate mapping.
					string lookupType = split[0].Last().ToString();
					string attributeName = split[0].Substring(0, split[0].Length - 1);
					string lookupValue = split[1].TrimStart('\'').TrimEnd('\'');
               
					switch (lookupType)
					{
						case "*": //Contains
						   // by = By.XPath(string.Format("//{0}[contains(@{1}, '{2}')]", selectorInfo.TagType, attributeName, lookupValue));
                            by = By.CssSelector(string.Format("{0}[{1}*='{2}']", selectorInfo.TagType, attributeName, lookupValue));
							break;
						case "^": //Starts with
							by = By.XPath(string.Format("//{0}[starts-with(@{1}, '{2}')]", selectorInfo.TagType, attributeName, lookupValue));
							break;
						case "$": //Ends with
							by = By.CssSelector(string.Format("{0}[{1}$='{2}']", selectorInfo.TagType, attributeName, lookupValue));
							break;
					}
				}
				else
				{
					//Something like input[id='value'] was found.  Maps directly.
					string attributeName = split[0];
					string attributeValue = split[1].TrimStart('\'').TrimEnd('\'');
					by = By.XPath(string.Format("//{0}[@{1}='{2}']", selectorInfo.TagType, attributeName, attributeValue));
				}
			}
			else
			{
				//No '=' found in selector, look up normally
				by = By.XPath(string.Format("//{0}[@{1}]", selectorInfo.TagType, toMatch));
			}

			if (by != null)
			{
				elements = searchContext.FindElements(by);
			}
			return elements;
		}

       
        public static IDictionary<string, string> ParseQueryString(this Uri uri)
            {
                Regex _regex = new Regex(@"[?|&](\w+)=([^?|^&]+)");
                var match = _regex.Match(uri.PathAndQuery);
                var paramaters = new Dictionary<string, string>();
                while (match.Success)
                {
                    paramaters.Add(match.Groups[1].Value, match.Groups[2].Value);
                    match = match.NextMatch();
                }
                return paramaters;
            }

        public static Uri Append(this Uri uri, params string[] paths)
        {
            //Usage: var url = new Uri("http://site.com/subpath/").Append("/part1/", "part2").AbsoluteUri;
            return new Uri(paths.Aggregate(uri.AbsoluteUri, (current, path) => string.Format("{0}/{1}", current.TrimEnd('/'), path.TrimStart('/'))));
        }
  
		#endregion --Private Static Members--

		#region --Helper Classes--

		private class MatchResult
		{
			#region --Enums--

			public enum MatchType
			{
				//None,
				Tag,
				TagName,
				XPath,
				Selector,
			}

			#endregion --Enums--

			#region --Ctor(s) & Setup--

			public MatchResult(string selector, MatchType type, Match regexMatch)
			{
				Type = type;
				Selector = selector;
				RegexMatch = regexMatch;
			}

			#endregion --Ctor(s) & Setup--

			#region --Public Properties--

			public Match RegexMatch
			{
				get; private set;
			}

			public string Selector
			{ 
				get;
				set;
			}

			public MatchType Type
			{
				get; private set;
			}

			#endregion --Public Properties--
		}

		private class SelectorInfo
		{
			#region --Ctor(s) & Setup--

			public SelectorInfo(Match selectorMatch)
			{
				string tagType = selectorMatch.Groups[1].Value;
				string selector = selectorMatch.Groups[2].Value;
				string toMatch = selectorMatch.Groups[3].Value;

				TagType = tagType;
				Selector = selector;
				ToMatch = toMatch;
			}

			#endregion --Ctor(s) & Setup--

			#region --Public Properties--

			public string Selector
			{
				get; private set;
			}

			public string TagType
			{
				get; private set;
			}

			public string ToMatch
			{
				get; private set;
			}

			#endregion --Public Properties--
		}


        
		#endregion --Helper Classes--
	}
}