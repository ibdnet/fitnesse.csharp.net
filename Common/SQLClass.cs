﻿using System;
using System.Data.SqlClient;


namespace fitnesse.common
{
    public class SQLClass
    {
        public SQLClass()
        {

        }

       

        /// <summary>
        /// Example: INSERT INTO table (Column1, Column2) " +  "Values ('string', 1)";
        /// </summary>
        /// <param name="insertString">insert command</param>
        /// <param name="dbname">database to connect</param>

        public void sqlExecute(string sqlstatement, string host, string dbname)
        {

          
            SqlConnection myconnection = null;
            SqlCommand myCommand = null;

            
            using (myconnection = new SqlConnection("Data Source=" + host + ";Initial Catalog=" + dbname + ";Persist Security Info=True;User ID=AprivaCore;Password=apriva^core"))
            {

                //  SqlConnection myconnection = GetConnection(dbname);
                myCommand = new SqlCommand(sqlstatement,
                                                        myconnection);

                try
                {
                    myconnection.Open();
                    if (myconnection.State == System.Data.ConnectionState.Open)
                        myCommand.ExecuteNonQuery();
                    else
                        return;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    myconnection.Close();
                }
                finally
                {
                    myconnection.Close();
                }
            }
        }

        public string sqlGetValue(string sqlquery, string host, string dbname)
        {
            SqlDataReader myReader = null;
            SqlConnection myconnection = null;
            string retval ="";



            using (myconnection = new SqlConnection("Data Source=" + host + ";Initial Catalog=" + dbname + ";Persist Security Info=True;User ID=AprivaCore;Password=apriva^core"))
            {

                //  SqlConnection myconnection = GetConnection(dbname);
                SqlCommand myCommand = new SqlCommand(sqlquery,
                                                         myconnection);
               
                try
                {
                    myconnection.Open();
                    if (myconnection.State == System.Data.ConnectionState.Open)
                    {
                        myReader = myCommand.ExecuteReader();

                        while (myReader.Read())
                        {

                            retval = myReader.GetString(0);
                            break;
                        }
                    }
                    else
                        return "";

                }

                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());

                }
                finally
                {

                    myconnection.Close();
                }
            }
            return retval;
        }




        public int sqlGetIntValue(string sqlquery, string host, string dbname)
        {
            SqlDataReader myReader = null;
            SqlConnection myconnection = null;
            int retval = 0;



            using (myconnection = new SqlConnection("Data Source=" + host + ";Initial Catalog=" + dbname + ";Persist Security Info=True;User ID=AprivaCore;Password=apriva^core"))
            {

                //  SqlConnection myconnection = GetConnection(dbname);
                SqlCommand myCommand = new SqlCommand(sqlquery,
                                                         myconnection);
                myconnection.Open();
                try
                {
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {

                        retval = myReader.GetInt32(0);


                    }

                }

                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());

                }
                finally
                {

                    myconnection.Close();
                }
            }
            return retval;
        }

        public bool IsConnected()
        {


            SqlConnection myconnection = null;
            SqlCommand myCommand = null;
            string dbname = "AprivaCore";
           
            string hostname =  CommonExtensions.GetHostName(GlobalTestProperties.baseUrl);

            using (myconnection = new SqlConnection("Data Source=" + hostname + ";Initial Catalog=" + dbname + ";Persist Security Info=True;User ID=AprivaCore;Password=apriva^core"))
            {

                //  SqlConnection myconnection = GetConnection(dbname);
                myCommand = new SqlCommand("SELECT 1",   myconnection);

                try
                {
                    myconnection.Open();
                    if (myconnection.State == System.Data.ConnectionState.Open)
                        return true;

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                return false;
            }
        }
            
    }
}