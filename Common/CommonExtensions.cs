﻿using System;
using System.Globalization;
using System.Net.Mime;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Linq;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Collections;
using System.Net.Mail;
using System.Reflection;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace fitnesse.common
{
    /// <summary>
    /// This class holds common classes and functions that are used throughout the entire automation framework
    /// </summary>
    public static class CommonExtensions
    {
        private enum Mode
        {
            Unknown,
            Test,
            Regular
        }
       
        /// <summary>
        /// Formats the string using the provided parameters.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="args">Args</param>
        /// <returns>
        ///   <see cref="System.String"/>
        /// </returns>
        /// <by>Dan Turco</by>
        /// <created>7/23/2012</created>
        public static string FormatWith(this string format, params object[] args)
        {
            return string.Format(CultureInfo.InvariantCulture, format, args);
        }

        /// <summary>
        /// Concatinates the relative URI to the given baseURI, ensuring the correct forward slash "/" lives between them.
        /// </summary>
        /// <param name="baseUri">The base URI.</param>
        /// <param name="relativeUri">The relative URI.</param>
        /// <returns></returns>
        /// <by>Chris Belford</by>
        /// <created>10/23/2012</created>
        public static string PathCombineRelativeURI(this string baseUri, string relativeUri)
        {
            string baseUriFixed = baseUri.EndsWith("/") ? baseUri : baseUri + "/";
            string relativeUriFixed = relativeUri.StartsWith("/") ? relativeUri.TrimStart("/".ToCharArray()) : relativeUri;
            return baseUriFixed + relativeUriFixed;
        }
  //  }

    
     
   
       /// <summary>
       /// Starts the process <paramref name="processName"/> 
       /// </summary>
       /// <param name="processName">The name of the process</param>
       /// <param name="processPath">the full path to the process executable</param>
       /// <param name="processExecutable">the name of the process executable</param>

        public static void startProcess(String processName, String processPath, String processExecutable)
        {

            if (processName.Length > 0)
            {
                Process[] AllProcesses = Process.GetProcessesByName(processName);

                if (AllProcesses.Length == 0)
                {
                    Console.WriteLine("Starting Process " + processName);

                    Process nProcess = Process.Start(processPath + processExecutable);

                    if (nProcess.Equals(null) == false)
                    {
                        while (nProcess.Responding == false)
                        {
                            Thread.Sleep(500);

                        }
                    }
                }
            }
        }
        /// <summary>
        /// Kills the running process specified by <paramref name="processName"/>
        /// </summary>
        /// <param name="processName">The process name to stop</param>
        /// <returns></returns>
        /// <by>Deborah DeVine</by>
        /// <created>04/01/2013</created>
        public static void main(string processName)
        {

            // string processName = pName;

            Process[] AllProcesses = Process.GetProcessesByName(processName);

            // check to kill the right process
            foreach (Process ExcelProcess in AllProcesses)
            {
                Console.WriteLine("Killing Process {0}", ExcelProcess.Id);

                ExcelProcess.Kill();
            }

            AllProcesses = null;
        }

        /// <summary>
        /// Checks the host specified by the <paramref name="hostname"/> to see if the port specified
        /// by <paramref name="portno"/> is in use
        /// </summary>
        /// <param name="hostname">The name of the host</param>
        /// <param name="portno">The port number to verify</param>
        /// <returns></returns>
        public static Boolean IsPortInUse(String hostname, int portno)
        {
            Boolean bReturn = false;
            IPAddress IP = Dns.GetHostAddresses(hostname)[0];
            try
            {
                TcpClient client = new TcpClient(new IPEndPoint(IP, portno));

            }
            catch (SocketException ex)
            {
                if (ex.SocketErrorCode == SocketError.AddressAlreadyInUse)

                    bReturn = true;
            }
            return bReturn;
        }  
   
        /// <summary>
        /// Writes a text string to a file
        /// </summary>
        /// <param name="path">the full path name of the file</param>
        /// <param name="outText">The string to write to the <paramref name="path"/>File</param>
        public static void OutToTextFile(String path, String outText)
        {
            Console.WriteLine("Writing to file {0}", path);
            using (FileStream fs = File.Create(path))
            {

                byte[] info = new UTF8Encoding(true).GetBytes(outText);
                fs.Write(info, 0, info.Length);
                fs.Flush();
                fs.Close(); 

            }


        } //end of outtotext

        /// <summary>
        /// Reads a directory for files with a common name and returns the next available name for the file by appending the
        /// next number to filename
        /// </summary>
        /// <param name="sPath">The path to search</param>
        /// <param name="sFileName">The file prefix or name</param>
        /// <returns>string</returns>
        /// <by>Deborah DeVine</by>
        public static string GetNextFileName(string sPath, string sFileName)
        {
            string[] allfiles;
            string sFPath = sPath + sFileName;
            int i = 0;

            // Console.WriteLine("Backup File {0}", sFullPath);
            if (File.Exists(sFPath))
            {
                allfiles = Directory.GetFiles(sPath);
                foreach (string nFile in allfiles)
                {
                    if (nFile.IndexOf(sFileName.Split('.')[0], 0) > 0)
                        i++;
                }

                sFPath = sPath + sFileName.Split('.')[0] + "_" + i + "." + sFileName.Split('.')[1];

                return sFPath;

            }
            else
                return sFPath;
        } //end of BackupFile()



        
        /// <summary>
        /// Reads a directory for files with a common name and returns the next available name for the file by appending the
        /// next number to filename
        /// </summary>
        /// <param name="sPath">The path to search</param>
        /// <param name="sFileName">The file prefix or name</param>
        /// <returns>string</returns>
        /// <by>Deborah DeVine</by>
        /// <created>07/18/14</created>
        public static string GetLastFileSaved(DateTime dtTime, string sPath, string sExt)
        {

            DirectoryInfo infodir = null;
            int i = 0;
            string lastFileName = "";
            // Console.WriteLine("Backup File {0}", sFullPath);
            //if (File.Exists(sFPath))
            try
            {
                 infodir = new DirectoryInfo(sPath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            if (infodir != null)
            {

                while (infodir.GetFiles(sExt).OrderBy(p => p.CreationTime).FirstOrDefault(x => x.CreationTime >= dtTime) == null)
                {
                    Thread.Sleep(1000);
                    i++;
                    if (i > 120)
                        break;
                }

                if (infodir != null)
                {
                    try
                    {
                        lastFileName = infodir.GetFiles(sExt).OrderBy(p => p.CreationTime).FirstOrDefault(x => x.CreationTime >= dtTime).Name;
                    }
                    catch (Exception) { };
                }
            }
             //Console.WriteLine("File Saved to {0}".FormatWith(lastFileName));
             return lastFileName;       
        } 
        


        /// <summary>
        /// Writes an input stream to a file
        /// </summary>
        /// <param name="input">input stream</param>
        /// <param name="output">output stream</param>
        public static void CopyStream(Stream input, Stream output)
        {

            byte[] buffer = new byte[8 * 1024];
            int len;

            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);

            }

            output.Flush();
            output.Close();
        }


        /// <summary>
        /// Gets the input stream for a single resource <paramref name="fileName"/> located in the <paramref name="assemblyName"/> folder
        /// </summary>
        /// <param name="fileName">Name of file in assembly</param>
        /// <param name="assemblyName">The name of the assembly</param>
        /// <returns>stream, for the file </returns>
        public static Stream GetResourceFileStream(string fileName, string assemblyName)
        {

            Assembly currentAssembly = Assembly.Load(assemblyName);

            string[] arrResources = currentAssembly.GetManifestResourceNames();

            foreach (string resourceName in arrResources)
            {

                if (resourceName.Contains(fileName))
                {
                    Console.WriteLine("Downloading resource: {0}", fileName);
                    return currentAssembly.GetManifestResourceStream(resourceName);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets all resources from the <paramref name="resourceFolder"/>
        /// </summary>
        /// <param name="testDir">The directory to write the resources to</param>
        /// <param name="resourceFolder">The name of the resource folder in the project</param>
        /// <param name="assemblyName">The assembly name</param>
        public static void getAllResources(string testDir, string resourceFolder, string assemblyName)
        {
            Assembly currentAssembly = Assembly.Load(assemblyName);
            //bool bResources = false;
            
            string[] arrResources = currentAssembly.GetManifestResourceNames();

            foreach (string resourceName in arrResources)
            {
               int fPos = resourceName.IndexOf(resourceFolder);
               // bResources = true;
                if (fPos > 0) //subdirectory found in resources folder
                {

                    fPos = fPos + resourceFolder.Length + 1; //remove the fully qualified namespace info
                    string fullpath = testDir + resourceName.Substring(fPos);
                    // Console.WriteLine("Downloading resource: {0} to {1}", resourceName.Substring(fPos), testDir);
                    if (!File.Exists(fullpath) || (File.GetLastWriteTime(fullpath) < File.GetLastWriteTime(currentAssembly.Location)))
                    {
                    using (Stream file = File.Create(fullpath))
                    {
                        
                        CopyStream(currentAssembly.GetManifestResourceStream(resourceName), file);
                    }
                }

                }
            }

        }

        [Obsolete ("Function is not used", false)]
        public static void DownloadFile(string Path, string filename, string assemblyName)
        {

            string fullpath = Path + filename;
   
            Stream input = GetResourceFileStream(filename, assemblyName);
            
            if (input != null)
            {
                
                try
                {   
                    using (Stream file = File.Open(fullpath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                    
                        CopyStream(input, file);
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: {0} encountered writing to file {1}", e, fullpath);
                }

            }

        }
        /// <summary>
        /// Creates a zip file using the IonicZip opensource class library
        /// </summary>
        /// <param name="fileNames">List of file names to zip</param>
        /// <param name="saveFileName">The name of the zip file to create</param>
        /// <param name="ZipDirName">The folder name within the zip file to create</param>
        public static void ZipFile(List<string> fileNames, string saveFileName, string ZipDirName)
        {
            using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
            {
                int count = 0;
                foreach (string file in fileNames)
                {

                    count += 1;
                   // Console.WriteLine("{0}: Adding file {1} to {2}", count, file, saveFileName);
                    zip.AddFile(file);
                    // zip.AddFile(file, ZipDirName);
                }
                // add the report into a different directory in the archive
                zip.Save(saveFileName);
            }
        }


        /// <summary>
        /// Gets the value of the environment <paramref name="keyname"/>
        /// </summary>
        /// <param name="keyname">Name of the environment variable to retrieve</param>
        /// <returns>string</returns>
        /// <by>Deborah DeVine</by>
        /// <created>04/01/2013</created>
        public static string GetEnvironment(string keyname)
        {

            IDictionary environmentVariables = System.Environment.GetEnvironmentVariables();

            foreach (DictionaryEntry de in environmentVariables)
            {
                if (de.Key.ToString().Equals(keyname))
                {
                    return de.Value.ToString();
                }
            }
            return "";
        }

        /// <summary>
        /// Executes a shell command synchronously. Open source routine 
        ///</summary>
        /// <![CDATA[http://stackoverflow.com/]]>
        /// <param name="command">string command</param>
        /// <returns>string, as output of the command.</returns>
        public static void ExecuteCommandSync(object command)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();
                // Display the command output.
                Console.WriteLine(result);
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException);
            }
        }
  //  }
    ///// <summary>
    ///// Opensource routine take from StackOverflow for generating random unique long numbers (greater than 32bit #'s)
    ///// <![CDATA[http://stackoverflow.com/a/10984975/238419]]> 
    ///// </summary>
    //public static class RandomExtensionMethods
    //{
        /// <summary>
        /// Returns a random long from min (inclusive) to max (exclusive)
        /// </summary>
        /// <param name="random">The given random instance</param>
        /// <param name="min">The inclusive minimum bound</param>
        /// <param name="max">The exclusive maximum bound.  Must be greater than min</param>
        public static long NextLong(this Random random, long min, long max)
        {
            if (max <= min)
                throw new ArgumentOutOfRangeException("max", "max must be > min!");

            //Working with ulong so that modulo works correctly with values > long.MaxValue
            ulong uRange = (ulong)(max - min);

            //Prevent a modolo bias; see http://stackoverflow.com/a/10984975/238419
            //for more information.
            //In the worst case, the expected number of calls is 2 (though usually it's
            //much closer to 1) so this loop doesn't really hurt performance at all.
            ulong ulongRand;
            do
            {
                byte[] buf = new byte[8];
                random.NextBytes(buf);
                ulongRand = (ulong)BitConverter.ToInt64(buf, 0);
            } while (ulongRand > ulong.MaxValue - ((ulong.MaxValue % uRange) + 1) % uRange);

            return (long)(ulongRand % uRange) + min;
        }
        /// <summary>
        /// Returns a random long from 0 (inclusive) to max (exclusive)
        /// </summary>
        /// <param name="random">The given random instance</param>
        /// <param name="max">The exclusive maximum bound.  Must be greater than 0</param>
        public static long NextLong(this Random random, long max)
        {
            return random.NextLong(0, max);
        }

        /// <summary>
        /// Returns a random long over all possible values of long (except long.MaxValue, similar to
        /// random.Next())
        /// </summary>
        /// <param name="random">The given random instance</param>
        public static long NextLong(this Random random)
        {
            return random.NextLong(long.MinValue, long.MaxValue);
        }

        public static string ReadAllText(string path)
        {
            string result;
            using (StreamReader streamReader = new StreamReader(path))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }
    //}
    ///// <summary>
    ///// The Mail class will search for the reports generated by the Nunit-Results tool.  The files are zipped using
    ///// the open source IonicZip class.  The report is then mailed to the userID specified in the backoffice.ini file
    ///// </summary>

        /// <summary>
        /// Sends the report for the test 
        /// </summary>
        ///<param name="testDataDir">Current Test Directory</param>
        ///<param name="userID">Email address to send report </param>
        public static void EmailReport(string reportTitle)
        {
            //string to = userID;
           string testDataDir = GlobalTestProperties.projectDir;
            List<string> reportFiles = new List<string>();
            string dtTimestamp = DateTime.Now.ToShortDateString().Replace(@"/", @"_").Replace(@"\", @"_") + "_" + DateTime.Now.ToShortTimeString().Replace(@":", "").Replace(@" ", "");
            IEnumerable<string> files = Directory.EnumerateFiles(testDataDir, "*.*", SearchOption.TopDirectoryOnly);
            string outfile = @testDataDir + "\\automation_results_" + dtTimestamp + ".zip";

            foreach (string file in files)
            {
                reportFiles.Add(file);
            }
           // reportFiles.Add(testDataDir + "\\index.html");
           // reportFiles.Add(testDataDir + "\\Apriva.BackOffice.Test.html");


            //zip up the file 

            ZipFile(@reportFiles, @outfile, "reports");

            if (File.Exists(@outfile))
            {


                MailMessage message = new MailMessage(GlobalTestProperties.userID, GlobalTestProperties.userID);
                message.Subject = reportTitle  + "Execution Report for {0}".FormatWith(DateTime.Now);
                message.Body = "Please See Attached Report For Test Results from the automation test run.";
               
                Attachment data = new Attachment(@outfile, MediaTypeNames.Application.Octet);
                ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(outfile);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(outfile);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(outfile);
                message.Attachments.Add(data);
                SmtpClient client = new SmtpClient(GlobalTestProperties.smtpServer);
            
                client.UseDefaultCredentials = true;

                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
                          ex.ToString());
                }
            }//send mail attachment   
        } //end of send report function

      
        //public static void WriteTestResultsToFile(string sfileName, TestResultCollection tResults)
        //{
        //    StreamWriter writer = new StreamWriter(sfileName);

                      
        //    foreach (TestResult result in tResults)
        //    {
        //        //format each parm
        //        string testname = ( result.TestName.Length > 50 ?  result.TestName.Substring(0,49) : result.TestName);
        //        string teststatus = (result.TestStatus.Length > 25 ? result.TestStatus.Substring(0, 24) : result.TestStatus);
        //        string testresults = (result.TestResults.Length > 100 ? result.TestResults.Substring(0, 99) : result.TestResults);           
        //        writer.WriteLine("{0,-50}{1,-25}{2,-100}", testname, teststatus, testresults);
                                      
        //    }
        //    // writer.WriteLine(sb.ToString());
        //    writer.Close();
        //}

       public static bool IsTestRunning()
        {
            Mode mode_;
                   
            Func<object, bool> NUnitAttribute = attr =>
              {
                  var type = attr.GetType();
                  if (type.FullName != null)
                  {
                      return type.FullName.StartsWith("nunit.framework", StringComparison.OrdinalIgnoreCase);
                  }
                  return false;
              };
           
            //static Func<StackTrace, IEnumerable<StackFrame> > GetStackFrames = callStack=> callStack.GetFrames() ?? new StackFrame[0];
            Func<StackTrace, bool> GetStackFrames = callStack =>                
                     (callStack.GetFrames() ?? new StackFrame[0]).SelectMany(stackFrame => stackFrame.GetMethod().GetCustomAttributes(false)).Any(NUnitAttribute);
                
            mode_ = GetStackFrames(new StackTrace()) ? Mode.Test : Mode.Regular;

            return mode_ == Mode.Test;

        }

       //public static  SqlConnection OpenConnection()
       //{

       //    SqlConnection connection;

       //    using ( connection = new SqlConnection(GlobalTestProperties.sql))
       //    {
       //        connection.Open();
       //        // Use the connection
       //    }
       //    return connection;

       //}

       public static void CallWithTimeout(Action action, int timeoutMilliseconds)
    {
        Thread threadToKill = null;
        Action wrappedAction = () =>
        {
            threadToKill = Thread.CurrentThread;
            action();
        };

        IAsyncResult result = wrappedAction.BeginInvoke(null, null);
        if (result.AsyncWaitHandle.WaitOne(timeoutMilliseconds))
        {
            wrappedAction.EndInvoke(result);
        }
        else
        {
            threadToKill.Abort();
            throw new TimeoutException();
        }
    }


       public static string LogTestStepResult(string stepName, string Expected, object Actual, string comparisonType)
       {
          string status = "";

           switch (comparisonType)
           {

               case "contains":
                   status = Actual.ToString().Contains(Expected.ToString()) ? "passed" : "failed";
                   break;
               default:
                   status = Actual.Equals(Expected) ? "passed" : "failed";
                   break;
           }
           Console.WriteLine( "Test: {0}  {1} - Expected : {2}, Actual : {3}".FormatWith(stepName, status ,Expected, Actual));
           return status;
       }

             


    public static string GenerateRandomID(string ID)
		{
            Random random = new Random();
			
            if (ID.StartsWith("RandomID("))
            {
                string temp1 = ID.Split('(')[1];
                int iLen = temp1.IndexOf(')');
                if (iLen > 0)
                {
                    int maxLength = Int32.Parse(temp1.Substring(0, iLen));
                    int partialSerialNumber = random.Next(1, 1000000000);
                    ID = partialSerialNumber.ToString().PadLeft(maxLength, '0').Substring(0,maxLength-1);
                   
    
                    //ID = NextLong(new Random(), maxLength-1, maxLength).ToString();//Guid.NewGuid().ToString().Replace("-", "").Substring(0, maxLength);
                    Console.WriteLine("Generated Random ID {0}", ID);
                }
            }
          
			
			return ID;
		}


    public static string GetHostName(string baseUrl)
    {

        string hostname = "";
        string[] urlparts = baseUrl.Split('.');
        if (urlparts[0].IndexOf(@"https:") >= 0)
        {
            string aibapp = urlparts[0].Substring(8).Replace("app", "") + "dbm2";
            hostname = aibapp + ".apriva.dev";

            if (hostname.IndexOf("dbm2") < 0) //possible IP was used instead of alias
                hostname = "172.20.5.158";


        }


        return hostname;

    }


   
    } 
    }// end of common extensions class
