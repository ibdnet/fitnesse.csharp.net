﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace fitnesse.common
{
    public abstract class GlobalTestProperties
    {

        public static string projectDir
        {
            get;
            set;
        }
        

        public static bool sendmail
        {
            get;
            set;
        }

        public static string baseUrl
        {
            get;
            set;
        }

        public static string host
        {
            get;
            set;
        }


        public static string port
        {
            get;
            set;
        }

        public static int timeout
        {
            get;
            set;

        }


        public static int Wait
        {
            get;
            set;
        }

        public static string smtpServer
        {
            get;
            set;
        }

        public static string ModalWaitWindow
        {
            get;
            set;
        }


        public static string userID
        {
            get;
            set;
        }



    }
}