﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;

namespace fitnesse.common
{
    /// <summary>
    /// Environment Setup Activites that are execute before and after the test suite run if placed in the 
    /// Text Fixture heading. Inherits from the TestActionAttribute class that is part of the Nunit 2.6.2 framework.
    /// </summary>
    /// <by>Deborah DeVine</by>
    /// <created>04/30/2013</created>


    public class SetUpEnvironment : ColumnFixture
    {

        /// <summary>
        /// Upon initialization, creates the Test configuration class; Creates the directory files structure and copies
        /// the test resources used from the resources folder
        /// </summary>
        /// <param name="sPath">The path to hold the test files and run reports</param>
        /// <param name="resourcePath">The name of the folder that contains the embedded resources that will be downloaded
        /// to the local driver</param>
        /// <param name="sIniFile">The name of the ini file to read user specific settings</param>
        /// 

        internal static Globals myGlobals = null;
        public SetUpEnvironment()
        {

            //TestElement.setBrowser(BrowserType.Remote, GlobalTestProperties.baseUrl, browser, version, platform);


            myGlobals = new Globals();
            

            //private Globals myGlobals = new Globals();



        }


        public override object GetTargetObject()
        {

            return myGlobals;


        }
        public class Globals : GlobalTestProperties
        {

            public Globals()
            {
                GlobalTestProperties.projectDir = Globals.projectDir;
                //GlobalTestProperties.iniFile = Globals.iniFile;
                //GlobalTestProperties.baseUrl = Globals.baseUrl;
                GlobalTestProperties.host = Globals.host;
                GlobalTestProperties.port = Globals.port;
                GlobalTestProperties.timeout = Globals.timeout;
                GlobalTestProperties.Wait = Globals.Wait;
                GlobalTestProperties.smtpServer = Globals.smtpServer;
                GlobalTestProperties.ModalWaitWindow = Globals.ModalWaitWindow;
                GlobalTestProperties.userID = Globals.userID;
                //GlobalTestProperties.loginGroup = Globals.loginGroup;
                //GlobalTestProperties.testFile = Globals.testFile;



            }
        }

    }

}//of of namespace
