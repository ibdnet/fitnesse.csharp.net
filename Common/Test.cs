﻿using System.Collections.Generic;
using OpenQA.Selenium;
using System.Linq;
using System.Xml.Linq;
using System;



namespace fitnesse.common
{
    public static class Test
    {

        private static IWebDriver _currentDriver = null;
        private static string _browser = "", _version = "", _platform = "";

        public static IWebDriver WebDriver
        {
            get { return (_currentDriver == null ? CreateDriver() : _currentDriver); }
            set { _currentDriver = value; }

        }

        public static void setBrowser(string browser, string version, string platform)
        {
            _browser = browser;
            _version = version;
            _platform = platform;

        }

        private static IWebDriver CreateDriver()
        {
            if (_currentDriver == null)
            {
                DriverFactory newDriver = new DriverFactory();
                _currentDriver = newDriver.CreateDriver(_browser, _version, _platform);
            }
            return _currentDriver;

        }


        public static void close()
        {
            try {
            System.Collections.ObjectModel.ReadOnlyCollection<string> handles = _currentDriver.WindowHandles;
            foreach (string handle in handles)
            {
                
                _currentDriver.SwitchTo().Window(handle).Quit();
                
            }
            } catch (Exception) {}

            _currentDriver = null;
        }
       


    

       


    }
       
   

} 