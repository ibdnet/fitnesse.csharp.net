﻿namespace fitnesse.common
{
    /// <summary>
    /// Class object used to hold data elements parsed from XML files
    /// </summary>
	public class Element
	{
        
		public string Name
		{
			get;
			set;
		}

		public string Value
		{
			get;
			set;
		}

        public string Req
        {
            get;
            set;
        }

        public int Len
        {
            get;
            set;
        }
       
		public Element()
		{

		}

		public Element(string name, string value)
			: this()
		{
			Name = name;
			Value = value;
		}


        

	}
}
