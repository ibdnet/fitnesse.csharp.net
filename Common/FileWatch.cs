﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;  // For the sleep functionality.
using System.IO;		 // For the File class.

namespace fitnesse.common
{
    
        

        public class FileWatchEventArgs : EventArgs
        {
            private readonly string _lastFileSaved;

            public FileWatchEventArgs(string lastFileSaved)
            {
                _lastFileSaved = lastFileSaved;
            }

            public string GetFileName 
            { get { return _lastFileSaved; } }


        }

        public delegate void FileWatchEventHandler(object sender, FileWatchEventArgs e);

        public class FileWatch
        {
            DateTime dtTime;
            string sPath;
            string sExt;
            private string _lastFileSaved = null;
            private string _currentFileSaved = null;

            public FileWatch(DateTime _dtTime, string _sPath, string _sExt)
            {

                dtTime = _dtTime;
                sPath = _sPath;
                sExt = _sExt;
            }

                      
            public event FileWatchEventHandler FileWatchEvent;

            protected virtual void OnFileChange(FileWatchEventArgs e)
            {
                if (FileWatchEvent != null)
                {                  
                   // Invokes the delegates. 
                    FileWatchEvent(this, e);
                }
            }

           

            public void MonitorFile()
            {

                while (true)
                {
                    try
                    {
                        _currentFileSaved = CommonExtensions.GetLastFileSaved(dtTime, sPath, sExt);
                        
                    }
                    catch (Exception e) { Console.WriteLine(e); }

                    if (_currentFileSaved != _lastFileSaved)
                    {
                        _lastFileSaved = _currentFileSaved;
                        OnFileChange(new FileWatchEventArgs(_currentFileSaved));
                       // return lastFileSaved;
                    }

                    //Sleep for a little
                    Thread.Sleep(250);
                   
                }
               

            }
        }
   }
