﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Configuration;
using System;

namespace fitnesse.common
{
    public class ElementCollection : List<Element>
	{
		public Element this[string name]
		{
			get
            {              
                 return this.FirstOrDefault(x => x.Name == name);                           
			}
		}


        public Element this[string value, int num]
        {
            get
            {
                return this.FirstOrDefault(x => x.Value == value);
            }
        }
      
		public ElementCollection()
		{

		}
        
        public List<Element> elements 
        {
            get { return this; }
            set { this.AddRange(value); }

        }
        public List<Element> pages
        {
            get { return this; }
            set { this.AddRange(value); }

        }

        
		public void EnsureValue(string name, string value)
		{
			Element element = this[name];
			if (element != null)
			{
				element.Value = value;
			}
			else
			{
				Add(new Element(name, value));
			}
		}
	}
}
