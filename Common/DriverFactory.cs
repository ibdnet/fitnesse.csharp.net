﻿using System;
using System.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;




namespace fitnesse.common
{
	/// <summary>
	/// 
	/// </summary>
	/// <by>Dan Turco</by>
	/// <created>7/20/2012</created>
	public class DriverFactory : IDriverFactory
	{
		/// <summary>
		/// Creates the driver.
		/// </summary>
		/// <param name="driverName">Name of the driver.</param>
		/// <param name="driverPath">Driver path</param>
		/// <returns>
		///   <see cref="OpenQA.Selenium.IWebDriver"/>
		/// </returns>
		/// <by>Dan Turco</by>
		/// <created>7/20/2012</created>
        /// <modified name="by">Deborah DeVine</modified>
        /// <modified name="date">04/30/2013</modified>
        /// <modified name="description">Added a Global timeout parameter which can be modified in the .ini file</modified>
        /// 
        private IWebDriver driver;

        public IWebDriver CreateDriver(string browser, string version, string platform)
		{
          

            int timeout = GlobalTestProperties.timeout;
       
			

            //DesiredCapabilities desiredCapabilites = null;
            string host = GlobalTestProperties.host;


           
            Func<DesiredCapabilities, IWebDriver>getDriver = desiredCapabilities => {
                string remoteServer = @"http://" + host + ":" + GlobalTestProperties.port + "/wd/hub";
                 
                 desiredCapabilities.SetCapability("idle-timeout", 90);
                //for saucelabs connections
                 desiredCapabilities.SetCapability("username", "ddevine"); // supply sauce labs username 
                 desiredCapabilities.SetCapability("accessKey", "ef1b2d8c-5723-4f34-a254-e54b4fd8bcb5");  // supply sauce labs account key
                 desiredCapabilities.SetCapability("name", "Fitnesse Test"); // give the test a name
                       
                 IWebDriver idriver = new RemoteWebDriver(new Uri(remoteServer), desiredCapabilities);

                    try{
                         idriver.Manage().Window.Maximize();
                   
                            idriver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(timeout));
                    }catch (Exception e){
                         Console.WriteLine(e);
                    }


                        return idriver;
                     };

             switch (browser) {


                     case "*iexplore":			

                        InternetExplorerOptions options = new InternetExplorerOptions();
                        options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                     
                      //  options.EnableNativeEvents= true;
                        options.AddAdditionalCapability("CapabilityType.ACCEPT_SSL_CERTS", true);
                        driver = new InternetExplorerDriver(GlobalTestProperties.projectDir, options);
                        //IWebDriver driver = new InternetExplorerDriver(driverPath);
                       
                        driver.Manage().Window.Maximize();           
                    //    driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(timeout));            
                        break;
				
                     case "*chrome":
                        ChromeOptions capabilities = new ChromeOptions();
                         capabilities.AddArguments(new string[] { "--start-maximized" ,"--silent" });

                         //capabilities.AddArguments("ignore-certificate-errors");
                         driver =new ChromeDriver(GlobalTestProperties.projectDir, capabilities);
                         driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromMilliseconds(2000));
                         break; 
               
                    case "chrome":
                         //Note: location of binaries has been added to the system path variable for the default directory of the Hub jar file
                         driver = getDriver( DesiredCapabilities.Chrome());
                         break;   
                         //.SetCapability(ChromeOptions.Capability, chromOptions);
                         //    driver= getDriver(DesiredCapabilities.Chrome());
                         //    break; 
                    case "firefox":
                             driver = getDriver( DesiredCapabilities.Firefox());
                              break;
                     case "iexplore":
                             driver = getDriver(DesiredCapabilities.InternetExplorer());
                             break;

                     default:
                             driver = getDriver(DesiredCapabilities.InternetExplorer());
                              break;
                }


             return driver;

        }

               
              
  
	}
}
