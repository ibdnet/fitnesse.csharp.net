﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;




namespace FitNesseCSharp
{
    public class AddDevice : ColumnFixture
    {
        private DeviceExtension newDevice = new DeviceExtension("add");

        //override to map Fitnesee table to AddCustomer Object
        public override object GetTargetObject()
        {
            return newDevice;
        }

       
    
    }
}
