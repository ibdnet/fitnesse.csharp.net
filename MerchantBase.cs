﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
    public abstract class MerchantBase
    {

        protected IWebDriver driver;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_Processors']")]
        protected IWebElement ddlProcessor;

        [FindsBy(How = How.CssSelector, Using = "span[id*='_lbl_ProcessorName']")]
        protected IWebElement lblProcessorName;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_FriendlyName']")]
        protected IWebElement input_friendlyName;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_MerchantNumber']")]
        protected IWebElement input_MerchantNumber;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_IndustryCode']")]
        protected IWebElement ddlIndustryCode;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_Country']")]
        protected IWebElement ddlCountry;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_Currency']")]
        protected IWebElement ddlCurrency;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_SoftDescriptor']")]
        protected IWebElement input_softDesc;

        //[FindsBy(How = How.CssSelector, Using = "input[id*='_txt_SoftDescriptor']")]
        //protected IWebElement input_softDesc;

        [FindsBy(How = How.CssSelector, Using = "div[id*='_pnl_CheckCardTypes']")]
        protected IWebElement cardTypesPanel;

        protected string cardTypesPanelSelector = "div[id*='_pnl_CheckCardTypes']";

        [FindsBy(How = How.LinkText, Using = "Select All")]
        protected IWebElement lnkSelectAll;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_btn_Save']")]
        protected IWebElement btn_Save;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_NextStep']")]
        protected IWebElement ddlNextStep;


        [FindsBy(How = How.CssSelector, Using = "div[id*='uc_MerchTerm_UpdateProgress1']")]
        protected IWebElement updateprogress;

       

        //
        //                <Element name="AddEditMerchantFriendlyNameRequiredPopupSelector" value="table[id*='_vce_rfvFriendlyName_popupTable']" />
        //                <Element name="AddEditMerchantMerchantNumberRequiredPopupSelector" value="table[id*='_vce_rfvMerchantNumber_popupTable']" />
        //                <Element name="AddEditMerchantClearAllLinkText" value="Clear All" />
        public string processor { get; set; }
        public string friendlyName { get; set; }
        public string merchantNumber { get; set; }
        public string industryCode { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string descriptor { get; set; }
        public string cardTypes { get; set; }
        public string nextStep { get; set; }

    }
}
