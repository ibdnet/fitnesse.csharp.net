﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp

    /// <summary>
/// Class for the Main BackOffice PAge.  This class will throw an exception of the title does not match "Apriva Back Office"
/// </summary>
/// <by>Deborah DeVine</by>
/// <created>06/27/2014</created>
{
    public abstract class TerminalBase
    {
        protected IWebDriver driver;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_Devices_DropDownList']")]
        protected IWebElement ddlDevice;

        [FindsBy(How = How.CssSelector, Using = "span[id*='_lbl_DeviceInfo_Label']")]
        protected IWebElement lbl_deviceAddressSpanSelector;

        [FindsBy(How = How.CssSelector, Using = "input[id*='txt_EmailAddress']")]
        protected IWebElement input_deviceEmailAddress;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_TerminalNumber']")]
        protected IWebElement input_terminalNumber;


        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_SoftDescriptor']")]
        protected IWebElement input_terminalSoftDescriptor;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_FriendlyName']")]
        protected IWebElement input_terminalFriendlyName;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_StoreNumber']")]
        protected IWebElement input_terminalStoreNumber;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_btn_Save']")]
        protected IWebElement btnSave;


        [FindsBy(How = How.CssSelector, Using = "span[id*='_lbl_DeviceInfo_Label']")]
        protected IWebElement DeviceAddressSpanSelector;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_MerchantAccount_DropDownList']")]
        protected IWebElement ddlMerchant;

        [FindsBy(How = How.CssSelector, Using = "span[id*='lbl_SaveConfirmation_Label']")]
        protected IWebElement lnlSaveConfirmation;

        [FindsBy(How = How.CssSelector, Using = "div[id*='uc_MerchTerm_UpdateProgress1']")]
        protected IWebElement updateprogress;

        //public variables for fitnesses table
        public string deviceAddress { get; set; }
        public string deviceFriendlyName { get; set; }
        public string emailAddress { get; set; }
        public string terminalNumber { get; set; }
        public string terminalFriendlyName { get; set; }
        public string terminalStoreNumber { get; set; }
        public string terminalSoftDescriptor { get; set; }
        public string merchantNumber { get; set; }

    }
}
