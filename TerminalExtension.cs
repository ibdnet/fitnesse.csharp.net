﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
    public class TerminalExtension : TerminalBase
        {

            

            public TerminalExtension()
            {
                driver = Test.WebDriver;
                PageFactory.InitElements(driver, this);
                if (ddlDevice == null)
                    throw new NoSuchWindowException("Terminal Page Not Displayed");
                else
                    Console.WriteLine("Terminal Page");

          
            }

            public Boolean AddEditTerminal()
            {

                 Boolean edit = false;


                //Func<IWebDriver, String> getSelectedDeviceAddress = (iDriver) => DeviceAddressSpanSelector.Text;
               
                    try
                    {
                        IWebElement lnkTerminal = this.driver.FindLink(CommonExtensions.GenerateRandomID(terminalNumber));
                        if (lnkTerminal != null)
                        {
                            edit = true;
                            this.driver.PerformActionAndWait(() => lnkTerminal.Click(), (idriver) => !updateprogress.IsVisible());
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);

                    }
                

                /* Complex logic for picking device from drop down using friendly name.  As friendly name is not unique, and device option text will change depending on
                 * if terminals are attached to the device, we need to loop selecting devices, matching partial friendly name, until we have the correct device selected.
                 * We decide that the correct device is selected by checking if the displayed device address is correct. */
                //System.Threading.Thread.Sleep(1000);
                ////driver.PerformActionAndWait(() => lnkAddTerminal.Click(), (currentDriver) => currentDriver.PageRefreshed());
                //Console.WriteLine(deviceAddress);
                ////set the Email address
                //if (!emailAddress.Equals(string.Empty))
                //{
                // driver.Wait(d => (ddlDeviceAddress != null), GlobalTestProperties.timeout, GlobalTestProperties.Wait);
                //    input_deviceEmailAddress.ClearAndSendKeys(emailAddress);
                //}
                //System.Threading.Thread.Sleep(1000);
                //Func<IWebDriver, String> getSelectedDeviceAddress = (iDriver) => iDriver.FindOne(DeviceAddressSpanSelector).Text;
                // PageFactory.InitElements(driver, this);
                Boolean correctDevice = false;

                if (ddlDevice != null)
                {

                    System.Threading.Thread.Sleep(3000);

                    //too fast on Chrome. Causing issues with timing
                    try
                    {

                        WebDriverWait wait = new WebDriverWait(this.driver, new TimeSpan(0, 0, GlobalTestProperties.timeout));
                        wait.Until((d) =>
                           correctDevice = DeviceAddressSpanSelector.Text.Contains(deviceAddress));
                        //return correctDevice;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }

                }

                //catch (Exception e)
                //{

                //    throw new NotFoundException("Device Address {0} not found ".FormatWith(deviceFriendlyName));
                //}
                //Func<IWebDriver, string> getSelectedDeviceAddressText = (idriver) => myoption.Text;

                ////bool correctDevice = getSelectedDeviceAddressText(driver).Contains(deviceAddress);
                //bool correctDevice = ddlDevice.GetSelectOptions().FirstOrDefault(x => x.Text.Contains(deviceAddress)) != null;
                if ( (!correctDevice) && (!edit)) //changing device not allowed in edit mode
                {

                   
                    IEnumerable<IWebElement> options = ddlDevice.GetSelectOptions();
                    foreach (IWebElement option in options)
                    {
                        if (option.Text.Contains(deviceFriendlyName))
                        {
                            //Store off the displayed device address
                            string displayedDeviceAddressTextBeforeChange = DeviceAddressSpanSelector.Text;

                            //Select option in drop down and wait until wait window selector is no longer displayed and displayed address has changed
                            driver.PerformActionAndWait(
                                () => ddlDevice.SelectValue(option.Text),
                                (idriver) => idriver.PageRefreshed()
                                    && (DeviceAddressSpanSelector.Text != displayedDeviceAddressTextBeforeChange));
                        }

                        //Get device address for newly selected device
                        string newDisplayedDeviceAddressText = DeviceAddressSpanSelector.Text;

                        correctDevice = newDisplayedDeviceAddressText.Contains(deviceAddress);
                        if (correctDevice)           
                            break;
                        
                    }
                }

                string errorMsg = correctDevice ? "" : "Failure selecting device";
                //CommonExtensions.GenerateRandomID(userName)
                //Generate and set friendly name
                if (correctDevice)
                {
                    //enter friendly name
                    if (!terminalFriendlyName.Equals(string.Empty))
                    {
                        terminalFriendlyName = CommonExtensions.GenerateRandomID(terminalFriendlyName);
                        input_terminalFriendlyName.ClearAndSendKeys(terminalFriendlyName);
                    }

                    //Generate and set terminal number
                    if (!terminalNumber.Equals(string.Empty))
                    {
                        terminalNumber = CommonExtensions.GenerateRandomID(terminalNumber);
                        input_terminalNumber.ClearAndSendKeys(terminalNumber);
                    }

                    //email address
                    if (!emailAddress.Equals(string.Empty))
                        input_deviceEmailAddress.ClearAndSendKeys(emailAddress);

                    //Set store number
                    if (!terminalStoreNumber.Equals(string.Empty)) 
                        input_terminalStoreNumber.ClearAndSendKeys(terminalStoreNumber);

                    //Set soft descriptor
                    if (!input_terminalSoftDescriptor.Equals(string.Empty))
                        input_terminalSoftDescriptor.ClearAndSendKeys(terminalSoftDescriptor);

                    //select merchant
                    this.driver.PerformActionAndWait(() => ddlMerchant.SelectValue(merchantNumber), (d) => (d.PageRefreshed() && !updateprogress.IsVisible()));
                    
                    //save
                    try
                    {
                        this.driver.PerformActionAndWait(() => btnSave.SafeClick(),
                                    (d) =>
                                    {
                                        System.Threading.Thread.Sleep(3000);
                                        return (d.ElementIsClickable(By.LinkText("Logout")) && !updateprogress.IsVisible() );
                                    });
                    }
                    catch (WebDriverTimeoutException e)
                    {

                        Console.WriteLine(e);
                        driver.StopThis();
                        return false;
                    }

                    if (lnlSaveConfirmation.IsVisible()) {
                        Console.WriteLine(lnlSaveConfirmation.Text);
                        return false;
                    }
                        

                }
                else
                    return false;

                //return driver.FindLink(terminalNumber) != null;
                return new MainPage() != null;

            }
        }
}
