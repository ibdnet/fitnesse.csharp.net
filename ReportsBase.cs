﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using fitnesse.common;
using OpenQA.Selenium;
using fit;
using System.Xml.Linq;
using OpenQA.Selenium.Support.PageObjects;

namespace FitNesseCSharp
{
    public class ReportBase
    {


        /// <summary>
        /// Performs the validations for BackOffice Reports
        /// </summary>
        /// <returns></returns>
        /// <by>Deborah DeVine</by>
        /// <created>02/12/2014</created>


        protected IWebDriver driver;

        [FindsBy(How = How.CssSelector, Using = "input[id*='ctl00_cphMain_btnLocateCustomer']")]
        protected IWebElement btnLocateCustomer;

        [FindsBy(How = How.LinkText , Using ="Download This Data In Excel Format")]
        protected IWebElement lnkDownloadExcelFile;


        [FindsBy(How = How.CssSelector, Using = "div[id*='_upprogMain']")]
        protected IWebElement inprogress;
        //[FindsBy(How = How.CssSelector, Using = "span[id*='lbl_NumTransRecords']")]
        //protected IWebElement numTransRecords;
        protected string lblNumTransRecords = "span[id*='lbl_NumTransRecords']";
 
        //protected static string _reportName = "";

        // protected static Uri reportUri;
      
        protected const int REPORT_TIMEOUT = 300; //allow time for some reports to run
        //protected string inprogress = "div[id*='_upprogMain']" ;


        //protected IDictionary<int, string> fieldHashMap = new Dictionary<int, string>() {
        //        {1, "BeginDate"},
        //        {2, "EndDate"},
        //        {3, "TransactionType"},
        //        {4, "Terminal"}
        //    }
        //       ;
        public string downloadDir { get; set; }
        public string reportName {get; set;}
        public string companyName {get; set;}
        public string searchBy  {get; set;}
        public string searchText { get; set; }
        public string beginDate {get;set;}
        public string endDate {get; set;}
        public string transactionType {get;set;}
        public string terminal {get; set;}
        
        #region --Public Static Members--

        protected IDictionary<string, string> GetReportParms(string Url, string reportPath, string reportArgs)
        {
            Uri newUri;
            IDictionary<string, string> reportParms = new Dictionary<string, string>();

            if (Uri.TryCreate(new UriBuilder(Url).Uri, reportPath, out newUri))
                reportParms = new Uri(newUri, @reportArgs).ParseQueryString();//create the Dictionary object
            else
                throw new ArgumentException("Unable to combine {0} with {1}".FormatWith(Url, reportPath));

            return reportParms;

        }

        private string buildQueryString () {

            StringBuilder queryString= new StringBuilder();

            if (!beginDate.Equals(string.Empty))
                queryString.Append("?BeginDate=" + beginDate);

            if (!endDate.Equals(string.Empty))
                queryString.Append("?EndDate=" + endDate);
                
            if (!transactionType.Equals(string.Empty))
                queryString.Append("?TransactionType" + transactionType);

            return queryString.ToString();

            //"?BeginDate=" + DateTime.Now.ToString("MMDDYYYY") + "?EndDate=" + DateTime.Now.ToString("MMDDYYYY")
                           //    + "?TransactionType=All"
        }
        public Boolean GenerateReport()
        {
            //IWebDriver currentDriver = TestElement.Browser.CurrentDriver;
            // Uri newUri;
            //reportUri =  reportPath.
            string reportArgs = buildQueryString();
            string Url = GlobalTestProperties.baseUrl;
            bool results = false;
            string reportPath = ReportData.Reports["ReportPath"].Value;
            IDictionary<string, string> reportParms = GetReportParms(Url, reportPath, reportArgs);

           // this.driver.PerformActionAndWait(() => this.driver.NavigateToRelativeUrl(Url, reportPath), (idriver) =>
              //  idriver.PageRefreshed());

            foreach (KeyValuePair<string, string> colName in reportParms)
            {
                try
                {
                    IWebElement element = driver.FindOne(ReportData.Reports[colName.Key].Value);
                    if (element != null)
                        this.driver.SetDataValue(ReportData.Reports[colName.Key].Value, colName.Value);
                    //if (this.driver.IsVisible(ReportData.Reports[colName.Key].Value))
                        
                }
                catch (Exception e) { Console.WriteLine(e); }
            }

            IWebElement searchButton = this.driver.FindOne(ReportData.Reports["Search"].Value);

            if (searchButton != null)
            {
                Console.WriteLine("Clicking on Search Button");

               
               // Func<IWebDriver, bool> tableDisplayed = (idriver) => idriver.FindOne(ReportData.Reports["Table"].Value).IsVisible();
                  

               // this.driver.PerformActionAndWait(() => searchButton.Click(), (idriver) =>
                   // {
                  try
                        {
                            searchButton.Click();
                            int i = 0;
                            string norecs = ReportData.Reports["NoRecords"].Value;
                            string rtable = ReportData.Reports["Table"].Value;

                            while (!results)
                            {
                                if (this.driver.FindText(norecs))
                                    return false;
                                else
                                {
                                    results = this.driver.FindOne(rtable).IsVisible() && !inprogress.IsVisible();//? true : false;
                                    i++;

                                    if (i > REPORT_TIMEOUT)
                                        return false;
                                    else
                                        continue;
                                }
                             }

                                
                             //idriver.Wait(tableDisplayed, REPORT_TIMEOUT, GlobalTestProperties.Wait);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            //throw new NotFoundException("No results found for report");
                           return false;
                        }
                    return results;

                    //}  ); //end of Lambda experssion
            
               
            }

            return results;
        }


        public Boolean VerifyReportValues(IDictionary<string, string> args)
        {
            //IWebDriver currentDriver = TestElement.Browser.CurrentDriver;
            //ModifyCustomerDeviceListPageTestData modifyCustomerTestData = new ModifyCustomerDeviceListPageTestData(testData);
            //string tableSelector = ReportData.Reports["Table"].Value;
            IWebElement reportTable = this.driver.FindOne(ReportData.Reports["Table"].Value); //should always be a table entry for reports
            Boolean rowFound = false;

            // int iCount = 0;
            // string[] tableHeadings = null;

            if (reportTable != null)
            {
                Console.WriteLine("Rows found. Beginning validations");
                IEnumerable<IWebElement> header = reportTable.FindAll("tr").FirstOrDefault().FindAll("th"); //header
                IDictionary<int, string> iheader = new Dictionary<int, string>();
                int l = 0;

                foreach (IWebElement heading in header)
                {

                    iheader.Add(l, heading.Text);
                    l++;

                }


                IEnumerable<IWebElement> rows = reportTable.FindAll("tr");

                foreach (IWebElement tablerow in rows)
                {
                    IEnumerable<IWebElement> dataRow = tablerow.FindAll("td");

                    if (dataRow.Count<IWebElement>() > 0)
                    {
                        using (var getnext = dataRow.GetEnumerator())
                        {

                            foreach (KeyValuePair<int, string> pair in iheader) //iterate through header
                            {
                                //Console.WriteLine("Key : {0} - Value : {1}", pair.Key, pair.Value);
                                int j = 0;

                                if (args.ContainsKey(pair.Value)) //then this column needs to be compared
                                {
                                    // Console.WriteLine("Value {0} found", pair.Value);
                                    while (getnext.MoveNext() && (j != pair.Key))
                                    {
                                        Console.WriteLine("Key = {0}, dataColumn={1}", j, pair.Value);
                                        // getnext.MoveNext();//iterate until data column is the same as header column
                                        j++;
                                    }

                                    Console.WriteLine("DataRow =  {0}, Expected {1}", getnext.Current.Text, args[pair.Value]);
                                    rowFound = getnext.Current.Text.Equals(args[pair.Value]);
                                    getnext.Reset();
                                }
                            }

                        }
                    } //Element count

                } //for each

            }
            return rowFound;

        }


        #endregion

        #region --ReportData--
        protected static class ReportData
        {

            public static ElementCollection Reports;

            public static void LoadReportPageData(string reportPage)
            {
                //XmlTextReader reader = new XmlTextReader(TestProperties.xmlDataFileName);

                XDocument xdoc = XDocument.Load(GlobalTestProperties.projectDir + @"\ReportPageElements.xml");
                //Console.WriteLine("Loading Report {0}", xdoc);
                Reports =
                    (from _reports in xdoc.Descendants("Page")
                     where _reports.Attribute("name").Value.Equals(reportPage)

                     select new ElementCollection
                     {

                         //  get all of the page element names
                         elements =
                           (from _fields in _reports.Descendants("Element")

                            select new Element
                            {
                                Name = _fields.Attribute("name").Value,
                                Value = _fields.Attribute("value").Value,
                                //Req = _fields.Attribute("req") == null ? "no" : _fields.Attribute("req").Value

                            }

                            ).ToList<Element>()


                     }).FirstOrDefault();

                //return newElement;
                xdoc = null;
            } //end of function
        }


    }

        #endregion

}