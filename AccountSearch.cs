﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
    public class AccountSearch : fitlibrary.DoFixture
    {

        private IWebDriver driver;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_SearchType_DropDownList']")]
        private IWebElement ddlSearchType;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_AccountSearchButton']")]
        private IWebElement btnSearch;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_Searchtxt']")]
        private IWebElement txtSearchText;

        [FindsBy(How = How.CssSelector, Using = "a[id*='_MerchantsAndTerminals'")]
        private IWebElement lnkMerchAndTerminals;

        [FindsBy(How = How.CssSelector, Using = "a[id*='_lnkb_AddNewTerminal'")]
        private IWebElement lnkAddTerminal;

        [FindsBy(How = How.CssSelector, Using = "a[id*='_hl_Devices'")]
        private IWebElement lnkDevices;


        [FindsBy(How = How.CssSelector, Using = "a[id*='Devices_lnkb_AddNewDevice'")]
        private IWebElement lnkAddNewDevice;

        //dynamic page elements
        private string ResultTableJquerySelector = ".CompanySearch";
        private string ModifyLinkSelector = "a[id*='_hlModify']";



        private IDictionary<string, int> sMap = new Dictionary<string, int>() {
                {"Company Name", 1},
                {"Company ID", 2},
                {"Login Username", 2}}
               ;

        //search delegate function
        private Func<IWebDriver, IWebElement, String, IWebElement, Boolean> startSearch = (idriver, element, text, editbox) =>
        {
            Boolean istrue = false;
            idriver.PerformActionAndWait(() => element.SelectValue(text), (dr) => { istrue = editbox.IsVisible(); return istrue; });
            return istrue;
        };

        public AccountSearch()
        {
            this.driver = Test.WebDriver;
            PageFactory.InitElements(driver, this);

            //wait for the Search Text to Display
            this.driver.Wait(d => txtSearchText != null, 10, 1);

        }


        public Boolean SearchByAndGoToAddTerminal(string searchBy, string searchText)
        {
            AddTerminal newTermPage = null;
            //this.driver.PerformActionAndWait(() => ddlSearchType.SelectValue(searchBy), (dr) => txtSearchText.IsVisible());

            if (startSearch(this.driver, ddlSearchType, searchBy, txtSearchText))
            {
                txtSearchText.ClearAndSendKeys(searchText);

                btnSearch.Click();

                IWebElement row = GetSearchResultRow(searchBy, searchText);

                if ((row != null) && (lnkMerchAndTerminals != null))
                {
                    if (ModifyFoundRow(row))
                    {

                        //Click on Goto Merch and Termins Link
                        this.driver.PerformActionAndWait(() => lnkMerchAndTerminals.SafeClick(), (idriver) =>
                            lnkAddTerminal != null);

                        this.driver.PerformActionAndWait(() => lnkAddTerminal.Click(), (idriver) =>
                            {
                                newTermPage = new AddTerminal();
                                return newTermPage != null;
                            });
                    }
                    else
                        throw new NotFoundException("Terminal Page Not Displayed");

                }
            }
            return newTermPage != null;

        }




        public Boolean SearchByAndGoToAddDevice(string searchBy, string searchText)
        {
            AddDevice newDevicePage = null;
            //this.driver.PerformActionAndWait(() => ddlSearchType.SelectValue(searchBy), (dr) => txtSearchText.IsVisible());
            if (startSearch(this.driver, ddlSearchType, searchBy, txtSearchText))
            {
                txtSearchText.ClearAndSendKeys(searchText);

                btnSearch.Click();

                IWebElement row = GetSearchResultRow(searchBy, searchText);

                if ((row != null) && (lnkDevices != null))
                {
                    if (ModifyFoundRow(row))
                    {

                        //Click on Goto Merch and Termins Link
                        driver.PerformActionAndWait(() => lnkDevices.SafeClick(), (idriver) =>
                           lnkAddNewDevice != null);

                        driver.PerformActionAndWait(() => lnkAddNewDevice.Click(), (idriver) =>
                        {
                            if (!idriver.IsVisible(GlobalTestProperties.ModalWaitWindow))
                            {
                                newDevicePage = new AddDevice();
                            }
                            return newDevicePage != null;
                        });
                    }
                    else
                        throw new NotFoundException("Device Page Not Displayed");

                }
            }
            return newDevicePage != null;

        }

        private IWebElement GetSearchResultRow(string searchBy, string searchText)
        {
            IWebElement row = null;
            try
            {
                IList<IWebElement> searchResults = GetSearchResultRows();
                row = searchResults.FirstOrDefault(resultRow => (resultRow.FindAll("td")[sMap[searchBy]].Text == searchText));
                //Assert.NotNull(row, "No search result exists with company name '{0}.'".FormatWith(companyName));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new NotFoundException("Unable to locate {0}".FormatWith(searchText));

            }
            return row;
        }


        private IList<IWebElement> GetSearchResultRows()
        {
            List<IWebElement> foundRows = null;
            IWebElement resultContainer = null;

            //AccountSearchPageTestData searchTestData = new AccountSearchPageTestData();

            driver.Wait((idriver) => { resultContainer = idriver.FindOne(ResultTableJquerySelector); return resultContainer != null; }, GlobalTestProperties.timeout, GlobalTestProperties.Wait);

            //IWebElement resultContainer = testData.Browser.CurrentDriver.FindOne(searchTestData.ResultTableCssClass);
            if (resultContainer != null)
            {
                foundRows = resultContainer.FindAll("tr").ToList();
                foundRows.RemoveAt(0); //Remove header row from search results
            }

            return foundRows ?? null;
        }

        private bool ModifyFoundRow(IWebElement row)
        {
            // AccountSearchPageTestData searchTestData = new AccountSearchPageTestData();
            IWebElement modifyLink = row.FindOne(ModifyLinkSelector);
            string customerID = modifyLink.GetAttribute("href").Split('=')[1];
            string targetUrl = GlobalTestProperties.baseUrl.PathCombineRelativeURI("Home/Customer/CustomerModify.aspx?CustomerID={0}".FormatWith(customerID));
            //Click the "Modify" link
         
            driver.PerformActionAndWait(() => modifyLink.Click(), (idriver) => lnkMerchAndTerminals != null);
           

            return (targetUrl.Equals(driver.Url,  StringComparison.OrdinalIgnoreCase));
        }

    }
}
