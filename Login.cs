﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp 
{
    /// <summary>
    /// Class for the Login Page.  This class will throw an exception of the title does not match "Login"
    /// </summary>
    /// <by>Deborah DeVine</by>
    /// <created>06/27/2014</created>
    public class Login : fitlibrary.DoFixture
    {

        protected IWebDriver driver;
        
        [FindsBy(How = How.CssSelector, Using = "input[id*='_UserName']")]
        protected IWebElement username;

        [FindsBy(How=How.CssSelector, Using ="input[id*='_Password']")] 
        protected IWebElement password;

        
        [FindsBy(How = How.CssSelector, Using = "input[id*='_LoginButton']")]
        protected IWebElement loginButton;


        [FindsBy(How = How.LinkText, Using = "Reset Password")]
        protected IWebElement lnkResetPwd;

        public Login()
        {
            driver = Test.WebDriver;
            PageFactory.InitElements(driver, this);
         
            driver.Wait(d => d.Title.Equals("Login"), GlobalTestProperties.timeout, GlobalTestProperties.Wait);
            
            if (!driver.Title.Equals("Login"))
                 throw new NoSuchWindowException("Login Page Not Found");
            else
                Console.WriteLine("BackOffice Login Page");
                
        }

        /// <summary>
        /// Performs the login with the username and password passed from the Fitnesse test
        /// </summary>
        /// <param name="_username">BackOffice username</param>
        /// <param name="_password">BackOffice Password</param>
        /// <returns><c>true</c> if the Main Page is Displayed ; <c>false</c>otherwise.</returns>
        /// <by>Deborah DeVine</by>
        /// <created>06/27/2014</created>
        public void EnterUsernameAndPassword(string _username, string _password) {

            username.ClearAndSendKeys(_username);
            password.ClearAndSendKeys(_password);

            
            driver.PerformActionAndWait(() => loginButton.Click(), (idriver) => !idriver.Title.Equals("Login")); //click on the login button

            Console.WriteLine("Clicked on Login Button");

            

        }

     
        public Boolean NavigateToHomePageWithUsernameAndPassword(string _username, string _password)
        {

            username.ClearAndSendKeys(_username);
            password.ClearAndSendKeys(_password);

            loginButton.Click(); //click on the login button

            Console.WriteLine("Clicked on Login Button");
            
            return (new MainPage() != null);


        }

        public Boolean ResetPassword()
        {
            lnkResetPwd.Click();
            return ((new ChangePassword()) != null);

        }

        
    }

    
}
