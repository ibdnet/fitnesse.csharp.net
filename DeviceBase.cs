﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;
using System.Threading;

namespace FitNesseCSharp
{
    public abstract class DeviceBase
    {

        protected IWebDriver driver;

        [FindsBy(How=How.CssSelector, Using = "select[id*='_drpl_ContractType']")]
        protected IWebElement ddlContractType;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_DeviceType']")]
        protected IWebElement ddlDeviceType;
        
        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_Manufacturer']")]
        protected IWebElement ddlManufacturer;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_PrimaryCarrier']")]
        protected IWebElement ddlPrimaryCarrier;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_SoftwareVersion']")]
        protected IWebElement ddlSoftwareVersion;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_FriendlyName']")]
        protected IWebElement txtFriendlyName;

        protected string friendlyNameSelector = "input[id*='_txt_FriendlyName']";

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_SerialNumber']")]
        protected IWebElement txtSerialNum;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_Identifier6']")]
        protected IWebElement txtRDPNumber;


        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_Identifier5']")]
        protected IWebElement txtMSISDN;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_Esn']")]
        protected IWebElement txtDeviceESN;

       // [FindsBy(How = How.CssSelector, Using = "input[id*='ccid']")]
        protected string ICCIDSelector;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_Status']")]
        protected IWebElement ddlStatus;


        //[FindsBy(How = How.CssSelector, Using = "a[id*='Devices_lnkb_AddNewDevice'")]
        protected string devicesSelector = "a[id*='_hl_Devices'";


        [FindsBy(How = How.CssSelector, Using = "input[id*='AddEditDevice_btn_Save']")]
        protected IWebElement btn_Save;

        [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_NextStep']")]
        protected IWebElement ddlNextStep;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_btn_Cancel']")]
        protected IWebElement btn_Cancel;

        [FindsBy(How = How.CssSelector, Using = "div[id*='_uc_Devices_upprogDevices']")]
        protected IWebElement UpdateProgress;

        [FindsBy(How = How.CssSelector, Using = "input[id*='btnOK']")]
        protected IWebElement btn_Ok;

          
        //[FindsBy(How = How.CssSelector, Using="[id*='_uc_Devices_upprogDevices']")]
        protected string deviceUpdateProgress = "div[id*='_uc_Devices_upprogDevices']";

        

        protected string deviceAddressPopup = "div[id*='pnlMpe']";
        protected string deviceAddressPopupBody = "div#Body";


        public string friendlyName {get; set;}
        public string contractType {get; set;}
        public string manufacturer {get; set;}
        public string deviceType {get; set;}
        public string primaryCarrier {get; set;}
        public string softwareVersion {get; set;}
        public string deviceStatus {get; set;}
        public string deviceICCID {get; set;}
        public string deviceMSISDN {get; set;}
        public string deviceSerialNum { get; set; }
        public string deviceESN {get; set;}
        public string deviceRDPNum { get; set; }
        public string deactivationReason {get; set;}
        public string nextStep { get; set; }
       

        protected Func<IWebDriver, string, string, int, bool> enterAndWait = (driver, elementName, sValue, wait) =>
        {
            int retry = 0;
            const int MAX_RETRY = 20;
            bool isValid = false;
            int waittime = wait * 1000;
            Thread.Sleep(waittime); //increase value in ini file for slower connections

            WebDriverExtensions.SeleniumExceptionRetry(() =>
            {
                IWebElement element = null;
                do
                {

                    //problem with the ICCD element changing case for some device types
                    element = driver.FindOne(elementName) == null ? driver.FindOne(elementName.ToUpper()) : driver.FindOne(elementName);
                    element.ClearAndSendKeys(sValue);
                    //enter ICCD
                    Thread.Sleep(1000);
                    retry++;

                } while ((element.GetValueAttribute().Equals("")) && (retry <= MAX_RETRY));

                if (!element.GetValueAttribute().Equals(""))
                    isValid = true;

            });
            return isValid;

        };

    //            <Element name="AddDeviceErrorMessageFieldPopupSelector" value="td[class*='_validatorcallout_error_message_cell']" />
    //            <Element name="AddDeviceErrorMessageCloseButtonSelector" value="td[class*='_validatorcallout_close_button_cell']" />
    //            <Element name="AddDeviceDeviceAddedModalPopupSelector" value="div[id*='pnlMpe']" />
    //    <Element name="AddDeviceDeviceUpdateProgress" value="div[id*='_uc_Devices_upprogDevices']" />
    //            <Element name="AddDeviceDeviceAddedModalPopupInfoSelector" value="div#Body" />
    //            <Element name="AddDeviceDeviceAddedModalPopupCloseButtonSelector" value="input[id*='btnOK']" />
				
    //            <Element name="AddDeviceSaveConfirmationSpanSelector" value="span[id*='_lbl_SaveConfirmation']" />
    //            <Element name="AddDeviceSerialNumberRequiredPopupSelector" value="table[id*='_rfvSerialNumber_popupTable']" />
    //        </Elements>
    //    </Page>
    //<Page name="AddDeviceLocationPage">
    //  <Elements>
    //    <Element name="AddDeviceLocationNameSelector" value="input[id*='LocationNameField']"/>
    //    <Element name="AddDeviceLocationIDSelector" value="input[id*='LocationIDField']" />
    //    <Element name="AddDeviceLocationAddressLine1Selector" value="input[id*='AddressLine1Field']" />
    //    <Element name="AddDeviceLocationAddressLine2Selector" value="input[id*='AddressLine2Field']" />
    //    <Element name="AddDeviceLocationCitySelector" value="input[id*='CityField']" />
    //    <Element name="AddDeviceLocationCountrySelector" value="select[id*='CountryDropDownList']" />
    //    <Element name="AddDeviceLocationStateSelector" value="select[id*='StateDropDownList']" />
    //    <Element name="AddDeviceLocationPostalCodeSelector" value="input[id*='PostalCodeField']" />
    //    <Element name="AddDeviceLocationDetailsSelector" value="textarea[id*='additionalDetailsField']" />
    //  </Elements>
    }
}
