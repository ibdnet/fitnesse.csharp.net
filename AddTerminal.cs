﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp

    /// <summary>
    /// Class for the Main BackOffice PAge.  This class will throw an exception of the title does not match "Apriva Back Office"
    /// </summary>
    /// <by>Deborah DeVine</by>
    /// <created>06/27/2014</created>
{
    public class AddTerminal : ColumnFixture
    {
        private TerminalExtension addTerm = new TerminalExtension();

        //override to map Fitnesee table to AddCustomer Object
        public override object GetTargetObject()
        {
            return addTerm;
        }
       
       
       
    }
}
