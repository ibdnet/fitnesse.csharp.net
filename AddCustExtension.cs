﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
    public class AddCustExtension : CustomerBase
    {


        private static string _password;


        public static string 
            GetPassword { get { return _password; } }

        public AddCustExtension()
        {
            //initialize the customer info object with the data from FitNesse
            // mycust = new CustomerInfo();
            driver = Test.WebDriver;

            //initialize the objects on the page
            PageFactory.InitElements(driver, this);

        }

        //This function returns the user password if add successful
        public string GetLogin()
        {

            if (!parentCompany.Equals(string.Empty))
            {


                driver.PerformActionAndWait(() => parentLink.SafeClick(), (currentDriver) => !updateprogress.IsVisible() && currentDriver.PageRefreshed());

                FindCompany locateCompany = new FindCompany();

                if (!locateCompany.SelectParentCompany(parentCompany))
                    throw new ElementNotVisibleException("Unable to set parent company");


            }

            if (!accountType.Equals(string.Empty))
            {

                driver.PerformActionAndWait(() => ddlAccountType.SelectValue(accountType), (currentDriver) =>
                    !currentDriver.IsVisible(GlobalTestProperties.ModalWaitWindow));
            }


            if (!customerID.Equals(string.Empty))
            {


                //if (customerID.StartsWith("RandomID("))
                //{
                //    string temp1 = customerID.Split('(')[1];
                //    int iLen = temp1.IndexOf(')');
                //    if (iLen > 0)
                //    {
                //        int maxLength = Int32.Parse(temp1.Substring(0, iLen));
                //        customerID = CommonExtensions.GenerateRandomID(maxLength);
                //        Console.WriteLine("Generated Random ID {0}", customerID);
                //    }
                //}
                txtCustomerID.ClearAndSendKeys(CommonExtensions.GenerateRandomID(customerID));
            }


            if (!companyName.Equals(string.Empty))
                txtCustomerName.ClearAndSendKeys(companyName);


            if (!addressLine1.Equals(string.Empty))
                txtAddress1.ClearAndSendKeys(addressLine1);

            if (!addressLine2.Equals(string.Empty))
                txtAddress2.ClearAndSendKeys(addressLine2);


            if (!city.Equals(string.Empty))
                txtCity.ClearAndSendKeys(city);


            if (!country.Equals(string.Empty))
            {
                driver.PerformActionAndWait(
                    () => ddlCountry.SelectValue(country),
                    (currDriver) => currDriver.PageRefreshed()
                        && !state.Equals(string.Empty) ? ddlState.DropDownHasElementWithDisplayText(state) : true);
            }


            if (!state.Equals(string.Empty))
                ddlState.SelectValue(state);


            if (!postalcode.Equals(string.Empty))
                txtPostalCode.ClearAndSendKeys(postalcode);


            if (!phonenumber.Equals(string.Empty))
                txtPhoneNumber.ClearAndSendKeys(phonenumber);

            if (!faxnumber.Equals(string.Empty))
                txtFaxNumber.ClearAndSendKeys(faxnumber);

            if (!email.Equals(string.Empty))
                txtEmail.ClearAndSendKeys(email);

            if (!firstName.Equals(string.Empty))
                txtFirstName.ClearAndSendKeys(firstName);


            if (!lastName.Equals(string.Empty))
                txtLastName.ClearAndSendKeys(lastName);

            if (!userName.Equals(string.Empty))
            {

                //if (userName.StartsWith("RandomID("))
                //{
                //    string temp1 = userName.Split('(')[1];
                //    int iLen = temp1.IndexOf(')');
                //    if (iLen > 0)
                //    {
                //        int maxLength = Int32.Parse(temp1.Substring(0, iLen));
                //        userName = CommonExtensions.GenerateRandomID(maxLength);
                //        Console.WriteLine("Generated Random ID {0}", userName);
                //    }
                //}
                if (userName.IndexOf("RandomID") >= 0)
                    userName = "Login_{0}".FormatWith(CommonExtensions.GenerateRandomID(userName));

                txtLoginName.ClearAndSendKeys(CommonExtensions.GenerateRandomID(userName));
            }
            if (!userRoles.Equals(string.Empty))
            {
                string[] roles = userRoles.Split(',');

                foreach (string role in roles)
                    SelectUserRoles(role.Trim());

            }

            ddlNextStep.SelectValue(nextStep);

            //save the record and trap the exception error
            string errorMsg = "";

            try
            {
                driver.PerformActionAndWait(() => btnSave.Click(), (idriver) => !updateprogress.IsVisible() && infoDivPanel.IsVisible());


            }
            catch (Exception e)
            {

                errorMsg = driver.WarningMessage();
                if (errorMsg.Equals(""))
                    errorMsg = e.ToString();

            }


            if (!errorMsg.Equals("")) throw new Exception(errorMsg);

            //Parse new account info out of the "Account Summary" popup
            //IWebElement accountSummaryPanel = driver.FindOne(pnlAccountSummary);
            IWebElement infoDiv = infoDivPanel.FindOne(pnlAccountSummaryInfo);
            //IWebElement closeButton = accountSummaryPanel.FindOne(closeButtonSelector);

            string loginName = infoDiv.GetLine(1, "\r\n").Trim();
            _password = infoDiv.GetLine(2, "\r\n").Trim();


            if (btnClose.IsVisible())
                driver.PerformActionAndWait(() => btnClose.Click(), (currDriver) => !btnClose.IsVisible());

            return loginName;

        }






    }
}
