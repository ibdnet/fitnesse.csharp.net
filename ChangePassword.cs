﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;


namespace FitNesseCSharp
{
    /// <summary>
    /// Class for the Reset Page that displays when a users password is reset by the Admin or a first time login
    /// </summary>
    public class ChangePassword : fitlibrary.DoFixture
    {
        IWebDriver driver;

        [FindsBy(How=How.CssSelector, Using="input[id*='_NewPassword']")]
        protected IWebElement newPassword;

        [FindsBy(How=How.CssSelector, Using="input[id*='_ConfirmNewPassword']")]
        protected IWebElement confirmPassword;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_ChangePasswordPushButton']")]
        protected IWebElement changePasswordButton;

        [FindsBy(How = How.CssSelector, Using = "input[id*='_CancelPushButton']")]
        protected IWebElement cancelButton;

        public ChangePassword()
        {
            driver = Test.WebDriver;
            //initialize the page objects
            PageFactory.InitElements(driver, this);
            if (newPassword == null)
                throw new NoSuchWindowException("Change Password Screen Not Displayed");


        }


        public Boolean ChangeToPassword(string password)
        {
            newPassword.ClearAndSendKeys(password);
            confirmPassword.ClearAndSendKeys(password);
            try
            {
                changePasswordButton.Click();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;

            }
            //should return to main page

            System.Threading.Thread.Sleep(5000);

            return true;
        }



        public Boolean ChangeToPasswordUsingAndAnswer(string password, string secretType = "", string secret = "")
        {
            newPassword.ClearAndSendKeys(password);
            confirmPassword.ClearAndSendKeys(password);

            changePasswordButton.Click();
            //should return to main page

            SecretPage secretPage = new SecretPage();

           

            if (secretPage != null)
                return  secretPage.ChangeSecretQuestion(secretType, secret);

            return false;



        }
       
    }
}
