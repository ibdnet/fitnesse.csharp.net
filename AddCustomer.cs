﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
    public class AddCustomer : ColumnFixture
    {
        private AddCustExtension newCust = new AddCustExtension();

        //override to map Fitnesee table to AddCustomer Object
        public override object GetTargetObject()
        {
            return newCust;
        }
       

    }
   }

