﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
    public class ResetUserPage : ColumnFixture


    {
        //public variables for Fit Test
        public string username;
        public string secretanswer;

        protected IWebDriver driver;

        [FindsBy(How = How.CssSelector, Using = "input[id*='txtUserName']")]
        protected IWebElement txtUserName;

        [FindsBy(How = How.CssSelector, Using = "input[id*='btnFindUser']")]
        protected IWebElement btnFindUser;

        [FindsBy(How = How.CssSelector, Using = "span[id*='_lblText_Label']")]
        protected IWebElement lblPage;

        protected string sAnswerSelector = "input[id*='txtAnswer']";
        protected string sButtonReset = "input[id*='btnReset']";
        protected string sNewPasswordSpan = "span[id*='lblResult_Label']";

        public ResetUserPage()
        {
            driver = Test.WebDriver;

            PageFactory.InitElements(driver, this);
            try
            {

                driver.Wait((currDriver)=> lblPage.Text.Equals(@"Reset / Unlock User"),GlobalTestProperties.timeout, GlobalTestProperties.Wait);
            }
            catch (Exception)
            {

                
               throw new NoSuchWindowException("Reset Page Not Found");
            }
        }

     

        /// <summary>
        /// Resets the password for the provided username.
        /// </summary>
        /// <param name="testData">The test data.</param>
        /// <returns></returns>
        /// <by>Deborah DeVine</by>
        /// <created>07/01/2014</created>
        public string ResetUserPassword()
        {
            
            if (txtUserName != null)
                txtUserName.ClearAndSendKeys(username);
            else 
                throw new NotFoundException("User Name Input Not Found");
               

           //Click the Locate User button
            driver.PerformActionAndWait(
                () => btnFindUser.Click(),
                (currDriver) => currDriver.FindOne(sAnswerSelector) != null);
        


            //enter the user's secret answer
            IWebElement txtSecret = driver.FindOne(sAnswerSelector);
            if (txtSecret != null)
                txtSecret.ClearAndSendKeys(secretanswer);
            else
                throw new NotFoundException("Secret Answer Input Not Found");

            /* Wait for the new user's password to be returned.  (There is a non-trivial wait while the password is rendered,
             * so we wait until the new password parent span has text in it else we run into a problem with no password at 
             * all being passed back. */
            IWebElement btnReset = driver.FindOne(sButtonReset);

            if (btnReset != null)
            {
                driver.PerformActionAndWait(
                    () => btnReset.Click(),
                    (currDriver) =>
                    {
                        IWebElement newPasswordSpan = driver.FindOne(sNewPasswordSpan);
                        return (newPasswordSpan != null) && !string.IsNullOrEmpty(newPasswordSpan.Text);
                    });
            }
            else
                throw new NotFoundException ("Reset Button Not Found");

            //Retrieve and return new password.
            string newPassword = driver.FindOne(sNewPasswordSpan).Text;
            return newPassword;
        }
    }
}
