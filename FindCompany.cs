﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp {

    public class FindCompany : fitlibrary.DoFixture
    {
        private IWebDriver driver;


        [FindsBy(How = How.CssSelector, Using = "input[id*='_txt_SearchCompanyName']")]
        private IWebElement txtCompanyName;

        [FindsBy(How=How.CssSelector, Using="input[id*='btn_SearchCompanyName']")]
        private IWebElement btnCompanySearch;

        [FindsBy(How=How.CssSelector, Using="input[id*='uc_AddCustomer_pnl_Close']")]
        private IWebElement btnClose;

        //Search strings for dynamic webpage elements
        private string selectParentPopup = "div[id*='uc_AddCustomer_pnl_SelectNewParent']";
        private string waitImageSelector = "div[id*='_UpdateProgress2']";

        public FindCompany()
        {
            driver = Test.WebDriver;
            //initialize the page objects
            PageFactory.InitElements(driver, this);

            driver.Wait(d => (d.PageRefreshed() && txtCompanyName.IsVisible()), GlobalTestProperties.timeout, GlobalTestProperties.Wait);

        }


        public Boolean SelectParentCompany(string ParentCompany)
        {

            if (driver.PageRefreshed()) 
            {
                if (txtCompanyName == null)
                    return false;
                else
                {
                    Console.WriteLine("Selecting Parent Company {0}", ParentCompany);
                    txtCompanyName.ClearAndSendKeys(ParentCompany);
                    try
                    {
                        driver.PerformActionAndWait(() =>
                        btnCompanySearch.SafeClick(), (currentDriver) => !currentDriver.IsVisible(waitImageSelector) && (currentDriver.FindElement(By.PartialLinkText(ParentCompany))) != null);



                        driver.PerformActionAndWait(() =>
                               driver.FindElement(By.CssSelector("span[title=\"Customer ID: " + ParentCompany + "\"]")).Click(),

                               (currentDriver) => !driver.IsVisible(selectParentPopup));
                    }
                    catch (Exception e)
                    {

                        Console.WriteLine(e);
                       //close popup if still visible
                        if (driver.IsVisible(selectParentPopup))
                        {

                            //close the popup
                            driver.PerformActionAndWait(() =>
                               btnClose.SafeClick(), (currentDriver) => (!driver.ElementExists(selectParentPopup)));

                            return false;
                        }
                        
                        return false;
                    }

                  
                   }
               }
            return true;
         } 

       } //end of Find company class
  
}
