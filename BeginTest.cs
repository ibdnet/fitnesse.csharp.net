﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using fitnesse.common;
using OpenQA.Selenium;
using fit;



namespace FitNesseCSharp
   
{
    /// <summary>
    /// Main entry point for the FitNesse Test. This class must be loaded in the Fitnesse test before calling any other classes in this project. See example
    /// Fitnesse login script which throws an exception because of an invalid password:
    /// 
    ///     Invalid Login Fitnesse Test
    ///     ==================================================================
    ///     !3 To change the Browser and Version under test, edit the line below
    ///     !|Begin Test|
    ///     |Launch Browser|chrome|On Version|34|Using Platform|Windows 7|
    ///     |Navigate To Url|https://aibapp155.aprivaeng.com:9443|
    ///     !|Login|
    ///     |check|Enter Username|autotest1|And Password|monkey|exception["Back Office Page Not Found"]|
    /// </summary>
    /// <by>Deborah DeVine</by>
    /// <created>06/27/2014</created>
   
    public class BeginTest  : fitlibrary.DoFixture
    {

        ///************uncomment the lines below and recompile to run as a console test for debugging scripts****************
        //static void Main()
        //{

        //    GlobalTestProperties.baseUrl = "https://aibapp155.aprivaeng.com";
        //    GlobalTestProperties.projectDir = @"C:\temp\BackOfficeTesting";
        //    Test.setBrowser("*chrome", "34", "Windows 7");
        //    IWebDriver driver = Test.WebDriver;
        //    driver.Navigate().GoToUrl(GlobalTestProperties.baseUrl);
        //    Login loginPage = new Login();

        //    loginPage.EnterUsernameAndPassword("ddevine1", "Apriva12");
        //    Thread.Sleep(1000);
        //    new MainPage().Logout();
        //    new StopTest().Close();

        //}

        public BeginTest()
        {
            //perform any necessary cleanup
            StopTest stop = new StopTest();
            stop.Close();
            stop = null;

        }
        /// <summary>
        /// Lanuches the browser specific by the Fitnesse test
        /// </summary>
        /// <param name="browser">Browser type <c>iexpore</c>, or <c>crome</>, or <c>firefox</c></c></param>
        /// <param name="version">The version of the browser</param>
        /// <param name="platform">Platform</param>
        public void  LaunchBrowserOnVersionUsingPlatform(string browser, string version, string platform)
        {
            Console.WriteLine(GlobalTestProperties.projectDir);
            try
            {
                Test.setBrowser(browser, version, platform);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            

        }


        /// <summary>
        /// Navigates to the url argument passed from the Fitnesse test
        /// </summary>
        /// <param name="Url">Fully qualified URL string</param>
        public void NavigateToUrl(string Url)
        {
            IWebDriver driver = Test.WebDriver;

            GlobalTestProperties.baseUrl = Url;
            driver.Navigate().GoToUrl(GlobalTestProperties.baseUrl);

      

        }
   

       /// <summary>
       /// Closes any active browsers and drivers and does some garbage cleanup. This should be called as 
        /// TearDown() test in Fitnesse <see cref="http://fitnesse.org/FitNesse.SuiteAcceptanceTests.SuiteResponderTests.SuiteTestResponders.SuiteSetUpAndTearDown"/>
       /// </summary>
       /// <by>Deborah DeVine</by>
       /// <created>06/27/2014</created>

        public class StopTest : fitlibrary.DoFixture
        {
            public void Close()
            {

                try
                {
                    if (Test.WebDriver != null) {
                        Console.WriteLine(Test.WebDriver.ConfirmAlertDialog(true)); //dismiss any open dialogs
                        
                        MainPage main = new MainPage();
                        if (main != null)
                            main.Logout();
                    }
                }
                catch (Exception)
                {
                    //Console.WriteLine(e);

                }
                finally
                {
                    TearDownTest();
                }


            }

            public void TearDownTest()
            {
                    Test.close();
                    GC.Collect();
                    GC.SuppressFinalize(this);
            
            }


        
        }
      
       
    }

               
    
}
    
