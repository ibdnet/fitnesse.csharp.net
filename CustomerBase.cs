﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;

namespace FitNesseCSharp
{
    
        /// <summary>
        /// Second part of the class which holds the webElements and properties of the customer class
        /// </summary>
        public abstract class CustomerBase     {

            protected IWebDriver driver;

            /* the AddCustomer web page elements*/

            [FindsBy(How = How.LinkText, Using = "Select New Parent")]
            protected IWebElement parentLink;

            [FindsBy(How = How.CssSelector, Using = "select[id*='drpl_Role_DropDownList']")]
            protected IWebElement ddlAccountType;

            [FindsBy(How = How.CssSelector, Using = "input[id*='cbl_A ccountTypeFlags_0']")]
            protected IWebElement chkloyaltyFlag;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_CustomerID']")]
            protected IWebElement txtCustomerID;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_CustomerName']")]
            protected IWebElement txtCustomerName;

            [FindsBy(How = How.CssSelector, Using = "select[id*='_drpl_Role_DropDownList']")]
            protected IWebElement ddlAccountTypeSelector;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_Address1']")]
            protected IWebElement txtAddress1;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_Address2']")]
            protected IWebElement txtAddress2;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_City']")]
            protected IWebElement txtCity;

            [FindsBy(How = How.CssSelector, Using = "select[id*='drpl_Country_DropDownList']")]
            protected IWebElement ddlCountry;

            [FindsBy(How = How.CssSelector, Using = "select[id*='drpl_State_DropDownList']")]
            protected IWebElement ddlState;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_PostalCode']")]
            protected IWebElement txtPostalCode;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_PhoneNumber']")]
            protected IWebElement txtPhoneNumber;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_FaxNumber']")]
            protected IWebElement txtFaxNumber;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_Email']")]
            protected IWebElement txtEmail;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_FirstName']")]
            protected IWebElement txtFirstName;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_LastName']")]
            protected IWebElement txtLastName;

            [FindsBy(How = How.CssSelector, Using = "input[id*='txt_LoginUserName']")]
            protected IWebElement txtLoginName;


            [FindsBy(How = How.CssSelector, Using = "input[id*='btn_Save']")]
            protected IWebElement btnSave;

            [FindsBy(How = How.CssSelector, Using = "select[id*='drpl_NextStep']")]
            protected IWebElement ddlNextStep;

            [FindsBy(How = How.CssSelector, Using = "div[id*='_uc_AddCustomer_pnl_UserRoles']")]
            protected IWebElement tblRoleSelector;

            [FindsBy(How = How.CssSelector, Using = "input[id*='btn_Cancel']")]
            protected IWebElement btnCancel;
            //[FindsBy(How = How.CssSelector, Using = ".validatorCalloutHighlight")]
            //IWebElement validHiLite;
            [FindsBy(How = How.CssSelector, Using = "div[id*='pnlMpe']")]
            protected IWebElement infoDivPanel;

            //[FindsBy(How = How.CssSelector, Using = "div[id#Body']")]
            //protected IWebElement infoDiv;
            protected string pnlAccountSummaryInfo = "div#Body";

            [FindsBy(How = How.CssSelector, Using = "input[id*='btnOK']")]
            protected IWebElement btnClose ;

            [FindsBy(How=How.CssSelector, Using = "div[id*='_UpdateProgress1']")]
            protected IWebElement updateprogress;

           // protected string closeButtonSelector = "input[id*='btnOK']";
            // [FindsBy(How = How.CssSelector, Using = "span[id*='_lbl_ParentAccount_Label'")]
            //IWebElement lblParentAccount;
           

            
            public string accountType { get; set; }
            public string customerID { get; set; }
            public string companyName { get; set; }
            public string parentCompany { get; set; }
            public string addressLine1 { get; set; }
            public string addressLine2 { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string state { get; set; }
            public string postalcode { get; set; }
            public string phonenumber { get; set; }
            public string faxnumber { get; set; }
            public string email { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string userName { get; set; }
            public string userRoles { get; set; }
            public string nextStep { get; set; }


            protected void SelectUserRoles(string checkboxLabelDisplayText)
            {
                // AddUserPage addUserPageTestData = new AddUserPage(testData);
                // IWebDriver currentDriver = testData.Browser.CurrentDriver;


                IWebElement checkboxToToggle = tblRoleSelector.GetInputFromLabelText(checkboxLabelDisplayText);

                if (checkboxToToggle != null)
                {
                    checkboxToToggle.SafeClick();
                    Console.WriteLine("Selected Role {0}", checkboxLabelDisplayText);
                        
                }


            }
           

        }

       

       
}
