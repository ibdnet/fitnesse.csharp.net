﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fit;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using fitnesse.common;using System.Text.RegularExpressions;

namespace FitNesseCSharp
{
    public class DeviceExtension : DeviceBase
    {

        private string edit;
        private string address = "";

        public DeviceExtension(string _edit)
        {
            this.driver = Test.WebDriver;

            //initialize the objects on the page
            PageFactory.InitElements(driver, this);

            if (btn_Save == null)
                 throw new NoSuchWindowException("Device Page Not Displayed");

            edit = _edit;
        }

        public string FillNewDeviceInfo()
        {

            if (ddlContractType != null)
            {

            //Select "Contract Type"
                if (!contractType.Equals(string.Empty))
                    this.driver.PerformActionAndWait(() => ddlContractType.SelectValue(contractType), 
                        (idriver) => ddlContractType.GetSelectedOption().Text.Equals(contractType));
                //this.driver.SelectOptionByDisplayText(addDevicePage.ContractTypeDropDownSelector, addDeviceValues.ContractType);

                //***THESE FIELDS FOR ADD ONLY

                if (edit.ToLower().Equals("add"))
                {
                    //Select "Manufacturer"
                    if (!manufacturer.Equals(string.Empty))
                        this.driver.PerformActionAndWait(
                            () => ddlManufacturer.SelectValue(manufacturer),
                            (idriver) => idriver.PageRefreshed() && !UpdateProgress.IsVisible()
                                && ddlDeviceType.DropDownHasElementWithDisplayText(deviceType));

                    //Select "Device Type"     
                    if (!deviceType.Equals(string.Empty))
                        this.driver.PerformActionAndWait(
                            () => ddlDeviceType.SelectValue(deviceType),
                            (idriver) => idriver.PageRefreshed()//driver.PageRefreshed()
                                && !UpdateProgress.IsVisible()
                                && ddlPrimaryCarrier.DropDownHasElementWithDisplayText(primaryCarrier));

                    //Select "Primary Carrier"
                    if (!primaryCarrier.Equals(string.Empty))
                        this.driver.PerformActionAndWait(
                            () => ddlPrimaryCarrier.SelectValue(primaryCarrier),
                            (idriver) => idriver.PageRefreshed() && !UpdateProgress.IsVisible()
                                && ddlSoftwareVersion.DropDownHasElementWithDisplayText(softwareVersion));
                } //end of add only fields



                /* Note:  Must fill in friendly name before selecting software version to prevent race condition where friendly name
                 * is filled as ajax call is made by changing software version, and return of ajax call clears friendly name. */

                //Select "Software Version"
                if (!softwareVersion.Equals(string.Empty))
                    this.driver.PerformActionAndWait(
                        () => ddlSoftwareVersion.SelectValue(softwareVersion),
                        (idriver) => idriver.AjaxCallComplete() && idriver.PageRefreshed() && !UpdateProgress.IsVisible());


                //Fill in friendly name
                if (!friendlyName.Equals(string.Empty))
                {
                    friendlyName = CommonExtensions.GenerateRandomID(friendlyName);


                    if (!enterAndWait(this.driver, friendlyNameSelector, friendlyName, GlobalTestProperties.Wait))
                        Console.WriteLine("Unable to set device FriendlyName to {0}", friendlyName);

                }

                /* Note:  There is a nasty race condition here where changing the software version will clear out the serial number; and no other screen value
                 * could be determined to be used in a wait condition (nothing else on the screen changes).  Changing the software version
                 * sends an ajax call, the return of which clears the serial number.  Sometimes waiting for the modal wait dialog to disappear handles the issue,
                 * but often not, the serial number is cleared, and saving the form results in a validation error.  That validation error must be 
                 * handled at a higher level when the form is saved.  */


                //string sICCID = !addDeviceValues.ICCD.Equals("") ? addDeviceValues.ICCD : (this.driver.FindOne(addDevicePage.ICCIDInputSelector) == null ? this.driver.FindOne(addDevicePage.ICCIDInputSelector.ToUpper()) :
                //     this.driver.FindOne(addDevicePage.ICCIDInputSelector)) != null ? edit.Equals("Add") ? (addDeviceValues.ICCDLen > 0) ?
                //     Device.GenerateSerialNumber(random, addDeviceValues.ICCDLen) : Device.GenerateSerialNumber(random, 19) : "" : "";
                //Fill in ICCID --05.29.2013.16.30.dld -if it is empty because of Ajax call try again
                if (!deviceICCID.Equals(string.Empty))
                {
                    //sICCID = Device.GenerateSerialNumber(random, 20);
                    //IWebElement ICCDInputSelector = this.driver.FindOne(addDevicePageTestData.ICCIDInputSelector);
                    deviceICCID = CommonExtensions.GenerateRandomID(deviceICCID);
                    if (!enterAndWait(this.driver, ICCIDSelector, deviceICCID, 1))
                        Console.WriteLine("Unable to set device ICCID to {0}", deviceICCID);
                }


                //Serial Number
                //string serialNum = !addDeviceValues.SerialNum.Equals("") ? addDeviceValues.SerialNum : this.driver.FindOne(addDevicePage.DeviceSerialNumberInputSelector) != null ? edit.Equals("Add") ? (addDeviceValues.SerialNumLen > 0) ?
                //    Device.GenerateSerialNumber(random, addDeviceValues.SerialNumLen) : Device.GenerateSerialNumber(deviceType) : "" : "";

                if (!deviceSerialNum.Equals(string.Empty))
                {
                    //string serialNumber = Device.GenerateSerialNumber(deviceType);
                    //this.driver.ClearAndSendKeys(addDevicePageTestData.DeviceSerialNumberInputSelector, serialNumber);
                    deviceSerialNum = CommonExtensions.GenerateRandomID(deviceSerialNum);
                    this.driver.PerformActionAndWait(
                      () => txtSerialNum.ClearAndSendKeys(deviceSerialNum),
                      (driver) => driver.PageRefreshed());
                }


                //ESN
                //string ESN = !addDeviceValues.ESN.Equals("") ? addDeviceValues.ESN : (this.driver.FindOne(addDevicePage.ESNSelector) != null) ?
                //    edit.Equals("Add") ? (addDeviceValues.ESNLen > 0) ? Device.GenerateSerialNumber(random, addDeviceValues.ESNLen) : Device.GenerateSerialNumber(random, 10) : "" : "";
                if (!deviceESN.Equals(string.Empty))
                {
                    deviceESN = CommonExtensions.GenerateRandomID(deviceESN);
                    this.driver.PerformActionAndWait(
                        () => txtDeviceESN.ClearAndSendKeys(deviceESN),
                        (idriver) => idriver.PageRefreshed());
                }

                //addDeviceValues.ESN = ESN;

                //MSISDN
                //string MSISDN = !addDeviceValues.MSISDN.Equals("") ? addDeviceValues.MSISDN : (this.driver.FindOne(addDevicePage.MSISDNSelector) != null) ?
                //    edit.Equals("Add") ? (addDeviceValues.MSISDNLen > 0) ? Device.GenerateSerialNumber(random, addDeviceValues.MSISDNLen) : Device.GenerateSerialNumber(random, 10) : "" : "";
                if (!deviceMSISDN.Equals(string.Empty))
                {
                    deviceMSISDN = CommonExtensions.GenerateRandomID(deviceMSISDN);
                    this.driver.PerformActionAndWait(
                        () => txtMSISDN.ClearAndSendKeys(deviceMSISDN),
                        (idriver) => idriver.PageRefreshed());
                }

                // addDeviceValues.MSISDN = MSISDN;

                //RDP Number
                //string RDPNum = !addDeviceValues.RDPNum.Equals("") ? addDeviceValues.RDPNum : (this.driver.FindOne(addDevicePage.RDPNumberSelector) != null) ?
                //    edit.Equals("Add") ? (addDeviceValues.RDPNumLen > 0) ? Device.GenerateSerialNumber(random, addDeviceValues.RDPNumLen) : Device.GenerateSerialNumber(random, 20) : "" : "";
                if (!deviceRDPNum.Equals(string.Empty))
                {
                    //IWebElement rdpnumElement = this.driver.FindOne(addDevicePageTestData.RDPNumberSelector); 
                    // string rdpnum = Device.GenerateSerialNumber(random, 20);
                    deviceRDPNum = CommonExtensions.GenerateRandomID(deviceRDPNum);
                    this.driver.PerformActionAndWait(
                        () => txtRDPNumber.ClearAndSendKeys(deviceRDPNum),
                        (idriver) => idriver.PageRefreshed());
                }

                // addDeviceValues.RDPNum = RDPNum;


                ////this.driver.SelectOptionByValue(addDevicePageTestData.StatusDropDownSelector, "-1"); //1 is Leave Inactive
                //AddEditLocationInfo(edit);

                ////Select "Status"
                //if (!addDeviceValues.Status.Equals(""))
                //{
                //    this.driver.PerformActionAndWait(
                //    () => this.driver.SelectOptionByValue(addDevicePage.StatusDropDownSelector, addDeviceValues.Status),
                //    (driver) => driver.PageRefreshed());

                //    if (edit.Equals("Edit"))
                //    {


                //        EditDevicePageTestData editDevicePage = new EditDevicePageTestData();
                //        if (this.driver.IsVisible(editDevicePage.DeactivationReasonsSelector))
                //        {
                //            EditDeviceValuesTestData editDeviceValues = new EditDeviceValuesTestData();
                //            this.driver.PerformActionAndWait(
                //           () => this.driver.SelectOptionByValue(editDevicePage.DeactivationReasonsSelector, editDeviceValues.DeactivationReason),
                //                   (driver) => driver.PageRefreshed());
                //            if (this.driver.IsVisible(editDevicePage.DeactivationCommentSelector))
                //            {

                //                if (!editDeviceValues.DeactivationComment.Equals(""))
                //                {
                //                    if (this.driver.IsVisible(editDevicePage.DeactivationCommentSelector))
                //                        this.driver.ClearAndSendKeys(editDevicePage.DeactivationCommentSelector, editDeviceValues.DeactivationComment);
                //                    else
                //                        Assert.Fail("Unable to enter Deactivation Reason");
                //                }
                //            }
                //        }
                //    }
                //}

                //Set "Next Step"
                //this.driver.SelectOptionByValue(addDevicePageTestData.NewDeviceNextStepDropDownSelector, "1"); //1 is Go to Device List

                this.driver.PerformActionAndWait(
                () => ddlNextStep.SelectValue(nextStep),
                (idriver) => idriver.PageRefreshed());



                //string errorMsg = "";

            

                //save new device
                this.driver.PerformActionAndWait(
                    () => btn_Save.Click(),
                    (idriver) => !idriver.IsVisible(deviceUpdateProgress) &&  idriver.IsVisible(deviceAddressPopup));

               // System.Threading.Thread.Sleep(2000);
                //currentDriver.PerformActionAndWait(
                // () => currentDriver.Click(addDevicePageTestData.SaveButtonSelector),
                //(driver) => driver.IsVisible(addDevicePageTestData.DeviceAddedModalPopupSelector));
                
                //Parse device address out of popup
                if (this.driver.IsVisible(deviceAddressPopup))
                {
                    //address = Device.ParseGeneratedDeviceAddress(currentDriver);
                    Console.WriteLine("closing Address Popup");
                
                   //  IWebElement infoDiv = this.driver.FindOne(deviceAddressPopup).FindOne(deviceAddressPopupBody);
                    IWebElement infoDiv = null;

                    WebDriverExtensions.SeleniumExceptionRetry(() => infoDiv = this.driver.FindOne(deviceAddressPopup).FindOne(deviceAddressPopupBody));
                    if (infoDiv != null)
                    {
                        Regex regex = new Regex(@"The generated address is (\d+)", RegexOptions.Compiled);
                        Match match = regex.Match(infoDiv.Text);
                        address = match.Groups[1].Value;
                    }

                    //Close popup
                    //try
                   /// {
                        //IWebElement popup = this.driver.FindOne(deviceAddressPopup);
                    try
                    {
                        driver.PerformActionAndWait(() => btn_Ok.SafeClick(), (idriver) => idriver.ElementIsClickable(By.CssSelector(devicesSelector)));
                    }
                    catch (WebDriverTimeoutException e )
                       
                    {

                        Console.WriteLine(e);
                        driver.StopThis();
                        return null;
                    }
                   // }
                 
                } //if address popup visible
           
                
                } //end of first if statement

            return address; 
            }
        

    }//end of class
       
    
}
